/**
 * @module util
 */

/**
 * Perform a post request with appropriate settings
 * @method
 * @param {string} url
 * @param {Object} body
 * @returns {Promise.<Response>}
 */
export const post = async function(url, body) {
	return fetch(url, {
		method: "POST",
		headers: {
			"Content-Type": "application/json"
		},
		credentials: "same-origin",
		body: JSON.stringify(body)
	});
};

/**
 * Perform a get request with appropriate settings
 * @method
 * @param {string} url
 * @returns {Promise.<Response>}
 */
export const get = async function(url) {
	return fetch(url, {
		method: "GET",
		credentials: "same-origin",
	});
};

/**
 * Calls callback when passed a keypress event with code 13 (Enter key)
 * @method
 * @param {function()} callback
 * @returns {function(Event)}
 */
export const onEnter = function(callback) {
	return (e) => {
		if (e.keyCode === 13) {
			callback();
		}
	}
};

/**
 * Pads a number with zeros to the length specified
 * @method
 * @param {number} number
 * @param {int} num - Minimum length of returned string
 * @returns {string}
 */
export const padNumber = function(number, num) {
	let numString = Math.floor(number).toString();
	let toAdd = num - numString.length;

	for (let i = 0; i < toAdd; i++) {
		numString = '0' + numString;
	}

	return numString;
};

/**
 * Template literal tag to pad all numbers to 2
 * @method
 * @param {string[]} strings
 * @param {...*} keys
 * @returns {string}
 */
export const pad2 = function(strings, ...keys) {
	let output = strings[0];

	for (let i = 0; i < keys.length; i++) {
		if (typeof(keys[i]) === "number") {
			output += padNumber(keys[i], 2) + strings[i+1];
		} else {
			output += keys[i] + strings[i+1];
		}
	}

	return output;
};

/**
 * Returns a string representation of the date and time of a Date object, or the current date is no Date object specified
 * @method
 * @param {?Date} date
 * @returns {string}
 */
export const datetimeToString = function(date) {
	if (date.constructor !== Date) {
		date = new Date(date);
	}
	return pad2`${date.getHours()}:${date.getMinutes()}:${date.getSeconds()} ${date.getDate()}/${date.getMonth() + 1}/${date.getFullYear()}`
};

/**
* Returns a string representation of the date only of a Date object, or the current date is no Date object specified
 * @method
 * @param {?Date} date
 * @returns {string}
 */
export const dateToString = function(date) {
	if (date.constructor !== Date) {
		date = new Date(date);
	}
	return pad2`${date.getDate()}/${date.getMonth() + 1}/${date.getFullYear()}`;
};

export class Progress {
	constructor(callback) {
		this.lastDisplayTime = 0;
		this.speedRefreshRate = 4; //Hz

		this.lastSpeed = 0;

		this.previous = 0;
		this.lastUpdate = null;
		this.callback = callback;

		this.update = this.update.bind(this);
	}

	update(e) {
		let speed;

		if (this.lastUpdate === null) {
			speed = 0;
		} else {
			let now = new Date();
			let deltaTime = (now - this.lastUpdate) / 1000;

			let deltaUpload = e.loaded - this.previous;

			speed = deltaUpload / deltaTime;
		}

		if ((new Date() - this.lastDisplayTime) < (1000 / this.speedRefreshRate)) {
			// Dont update speed
			speed = this.lastSpeed;
		} else {
			this.lastDisplayTime = new Date();
			this.lastSpeed = speed;
		}

		this.lastUpdate = new Date();
		this.previous = e.loaded;

		this.callback(e.loaded, e.total, e.loaded/e.total, speed)
	}

	static bytesToString(num) {
		if (!num) {
			return 'NaN';
		}

		let suffix = 'B';

		if (num > 1024) {
			suffix = 'KB';
			num = num / 1024;
		}

		if (num > 1024) {
			suffix = 'MB';
			num = num / 1024;
		}

		return num.toFixed(2) + suffix;
	}
}

export function preventDefaultLeftClick(e) {
	if (e.button === 0) {
		e.preventDefault();
	}
}