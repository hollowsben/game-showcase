export default function games(state = [], action) {
	let [prefix, type] = action.type.split(":");

	if (prefix !== "games") {
		return state;
	}

	switch (type) {
		case "set":
			return action.data;
			break;

		default:
			return state;
			break;
	}
}