import user from "./user";
import games from "./games";
import view from "./view";
import comments from "./comments";

export default { user, games, view, comments };