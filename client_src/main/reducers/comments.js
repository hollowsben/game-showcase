export default function comments(state = [], action) {
	let [prefix, type] = action.type.split(":");

	if (prefix !== "comments") {
		return state;
	}

	switch (type) {
		case "set":
			return action.data;
			break;

		default:
			return state;
			break;
	}
}