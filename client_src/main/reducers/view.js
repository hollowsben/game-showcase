export default function user(state = [], action) {
	let [prefix, type] = action.type.split(":");

	if (prefix !== "view") {
		return state;
	}

	switch (type) {
		case "set":
			return action.data;
			break;

		default:
			return state;
			break;
	}
}