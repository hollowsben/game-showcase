/**
 * Template literal tag to remove leading and trailing characters
 * @param strings
 * @returns {string}
 */
function f (strings) {
	return strings[0].slice(1, -1);
}

export let registerInfo = f`
Registration is restricted to invited individuals only. You can still comment without an account.
`;

export let tos = f`
SSA Game Showcase Terms of Service

0 Definitions

	You: You, a party entering into this contract.
	We, us: The owner(s) and/or operator(s) of the site.
	(The/this) site, website: The website located at ssa.hollows.cloud and services relating to the operation of said website.


By using this site you agree to comply with the following terms, and that you have the authority and capacity to enter into these terms.
If you do not agree to these terms you are not authorized to access or in any way use this site.

1 Privacy Policy
	By using this site you agree to the following privacy policy

	1.1 Cookies
		This website uses cookies in order to identify users.

	1.2 Google Analytics
		Identifying information about your browser and your movements through this site will be tracked by the third-party service Google Analytics

	1.3 Accounts
		By registering an account you give us permission to retain all submitted information for the purposes of identifying you
		Additionally we will supply your name and organisation to all users when they view content submitted by or associated with you

	1.4 Anonymous Submissions
		If you submit data to this site without a registered account your ip address and time of submission will be logged in order to reduce spam and abuse.


2 Accounts

	2.1 Account Creation

		By creating an account you verify that you have read, understood, and agreed to these terms.
		You ensure that
			- All information provided during registration is accurate and
			- If you used a registration code, that you are authorised to use that code

	2.2 Email Addresses

		By providing an email address you ensure that
			- You own or are otherwise legally entitled to access that email address and
			- You check that email address regularly

		By providing an email address you give us permission to directly or indirectly contact you about
			- The operation of this site
			- Any changes to these terms
			- Matters relating your security in relation to this site


3 Access

	Subject to these terms we grand you a non-transferable, non-exclusive, revocable, limited license to access this Site for the following purposes in accordance with these terms

		-	To view and play games submitted to the site
		-	To provide feedback on games submitted to the site

	Provided you have registered an account in accordance with these terms you are also granted access for the following purposes

		-	To submit games to the site
		-	To manage an organisation

	3.1 Restrictions

		-	You shall not sell, rent, lease, transfer, assign, distribute, host, or otherwise commercially exploit the site
		-	You shall not attempt to manipulate the site in any way not in accordance with normal operations

	3.2 Exploits
		All exploits, bugs, or other vulnerabilities discovered should be reported immediately to the administrator.
		Exploitation of the site is forbidden.


4 Support
	You agree that we are not obligated to provide you with any support in connection with this site


5 User Content

	User Content refers to any information or content submitted by a user to this site. You are exclusively responsible for your User Content.

	By submitting User Content you certify that it does not violate these terms
	You may not represent or imply that we provide, sponsor, or endorse your User Content
	We are not obligated to retain submitted content and may delete it at any time

	By submitting User Content your grant us a revocable, nonexclusive, royalty-free worldwide license to distribute and publicly display your content for any purpose.

	5.1 Removing User Content
		If you wish to remove submitted content you may do so through the provided interfaces or by contacting the administrators
		We are not obligated to remove content at the behest of anyone except the organisation administrator or verified submitter.

		If you provided fraudulent identification when submitting content we cannot verify the sender and will not remove it on request.

		We may remove any content at any time for any reason without explanation.

	5.2 Acceptable Content
		You may not submit any content that is illegal, defamatory, abusive, or not relevant to the site.
		
6 Updating this Policy
	This policy may be updated at any time with no notice, with changes coming into effect immediately.
	The updated policy will be available to view at the same place a the prior policy.
`;


export let intro = f`
This is a backup copy of the Super Street Arcade Game Showcase. The original server has since been shut down.
I only have my game to add here, I don't possess the original game files for the other teams.
This serves as an example for my portfolio, feel free to look around.

== Original Welcome Text Below ==

Welcome to the Super Street Arcade Game Showcase!
	
Displayed here are games currently in development by students in the hope that they will one day be publicly hosted on the Super Street Arcade.

But before this happens they need playtesting! So please, play these games and leave your valuable feedback.
`;
