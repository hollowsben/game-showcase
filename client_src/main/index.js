import { h, render } from "preact";
import { get } from "../common/util";

import { Provider } from "preact-redux";
import store from "./store";

import App from "./Components/App";

//Todo, remove for prod
require("preact/devtools");

window.onload = async () => {

	let updateState = getFuncUpdateState(store);
	await updateState();

	render(
		<Provider store={store}>
			<App updateState={updateState}/>
		</Provider>,

		document.getElementById("root")
	);
};

/**
 * Returns a function that when called updates the state
 * @param {store} store
 * @returns {function()}
 */
function getFuncUpdateState(store) {
	return async () => {
		let response, data;

		// User Data
		response = await get("/api/user_details");
		data = await response.json();

		store.dispatch({
			type: "user:set",
			data,
		});

		// Game List
		response = await get("/api/games");
		data = await response.json();

		store.dispatch({
			type: "games:set",
			data,
		});
	}
}