import { h, Component } from "preact"
import { get } from "../../common/util";
import deepEqual from "deep-equal";

class GameEditSelect extends Component {
	constructor() {
		super();

		this.state = {
			games: [],
		};

		this.getGameList = this.getGameList.bind(this);
		this.renderListItem = this.renderListItem.bind(this);
		this.selectGame = this.selectGame.bind(this);
	}

	componentWillMount() {
		this.getGameList();
	}

	async getGameList() {
		let response = await get("/api/games/editable");
		if (response.ok) {
			let data = await response.json();
			if (!deepEqual(this.state.games, data))
			this.setState({
				games: data,
			})
		} else {
			//todo communicate error
		}
	}

	selectGame(id) {
		this.props.dispatch({
			type: "view:set",
			data: {
				view: "game_manager",
				id,
			},
		})
	}

	renderListItem(game) {
		return (
			<div class="list-item" onClick={() => {this.selectGame(game.id)}}>
				{game.name}
			</div>
		);
	}

	render() {
		this.getGameList();

		if (!this.props.store.user.user) {
			return <div class="err">Access Denied - Please log in</div>
		}

		let newGameButton = null;
		if (this.props.store.user.user.role === "admin" || this.props.store.user.user.is_admin) {
			newGameButton = <button onClick={() => {
				this.props.dispatch({
					type: "view:set",
					data: {
						view: "add_game",
					}
				});}
			}>New Game</button>;
		}

		return (
			<div class="game-edit-list">
				<h2>Select game to edit</h2>
				{newGameButton}
				{this.state.games.map(this.renderListItem)}
			</div>
		);
	}
}

export default GameEditSelect;