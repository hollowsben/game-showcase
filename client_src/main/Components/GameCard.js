import { h, Component } from "preact";
import { dateToString } from "../../common/util";
import App from "./App";

/**
 * Game Info Card Component
 * @extends Component
 */
class GameCard extends Component {

	constructor() {
		super();

		this.playGame = this.playGame.bind(this);
		this.getUrl = this.getUrl.bind(this);
	}

	/**
	 * Switches view to the game viewer and loads the game assigned to this component
	 */
	playGame(e) {
		if (e.button === 0 ) {
			// Left click
			e.preventDefault();

			let view = {
				view: "viewer",
				gameId: this.props.info.id,
				info: this.props.info,
			};

			this.props.dispatch({
				type: "view:set",
				data: view,
			})
		}
	}

	getUrl() {
		let view = {
			view: "viewer",
			gameId: this.props.info.id,
			info: this.props.info,
		};

		return App.viewToUri(view);
	}

	/**
	 * Renders the component
	 * @param {Object} props
	 * @returns {XML}
	 */
	render(props) {
		let info = props.info;
		let classes = "no-a game-card ";
		let private_label = null;

		if (info.hasOwnProperty("is_public") && !info.is_public) {
			classes += "private ";
			private_label = <span class="private-label">[Private]</span>
		}

		let url = this.getUrl();

		return(
			<a href={url} onClick={this.playGame} class={classes}>
				<div class="info">
					<span class="title">{info.name}</span>
					<span class="authors">{info.authors}</span>
					<span class="organisation">{info.organisation_name}</span>
				</div>

				<span class="date">Updated {dateToString(info.date_updated)}</span>
				{private_label}

				<p class="description">{info.description.split('\n')[0]	/*first line only*/}</p>
			</a>
		)
	}
}

export default GameCard;