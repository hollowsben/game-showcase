import { h, Component } from "preact";
import Comments from "./Comments";
import Votes from "./Votes";
import { get } from "../../common/util";
import deepEqual from "deep-equal";

/**
 * Game viewer
 * @extends Component
 */
class Viewer extends Component {
	constructor() {
		super();

		this.state = {
			info: {},
		};

		this.getGameInfo = this.getGameInfo.bind(this);
	}

	/**
	 * Returns the url of the game viewer
	 * @param {int} id - Game id
	 * @returns {string} - Viewer URL
	 */
	getSrcUrl(id) {
		return `${window.location.origin}/viewer/viewer.html#${id}`;
	}

	/**
	 * Closes the viewer
	 */
	close() {
		this.props.dispatch({
			type: "view:set",
			data: {
				view: "home"
			}
		});
	}

	async getGameInfo() {
		let response = await get(`/api/game/${this.props.id}/info`);
		if (response.ok) {
			let data = await response.json();

			if (!deepEqual(this.state.info, data)) {
				this.setState({
					info: data,
				})
			}
		}
	}

	/**
	 * Render the component
	 * @param {Object} props
	 * @returns {XML}
	 */
	render(props, state) {
		this.getGameInfo();

		document.getElementsByTagName("title")[0].text = `${state.info.name} | Game Showcase`;

		return (
			<div class="viewer-container">
				<a class="close" onClick={this.close.bind(this)}>Close</a>

				<iframe class="viewer"
					scrolling="no"
					width="576px" height="432px"
					src={this.getSrcUrl(props.id)} />

				<div class="game-info">
					<div class="top-bar">
						<div class="meta-info">
							<h2 class="title">{state.info.name}</h2>
							<span class="attribution">{state.info.organisation} - {state.info.authors}</span>
						</div>

						<Votes store={props.store} gameId={props.id}/>
					</div>

					<hr />
					<p class="description">{state.info.description}</p>
					<div style="text-align: right"><a href={`/viewer/fullscreen.html#${props.id}`} style="color: #666666">Open Fullscreen Viewer</a></div>
				</div>

				<Comments dispatch={props.dispatch} store={props.store} gameId={props.id} />
			</div>
		)
	}
}

export default Viewer;
