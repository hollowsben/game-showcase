import { h, Component } from "preact";
import { post } from "../../common/util";

import { registerInfo } from "../strings";

/**
 * Registration form
 * @extends Component
 */
class Registration extends Component {
	constructor() {
		super();

		this.state = {
			errors: "",
		};

		this.registerCallback.bind(this);
	}

	async registerCallback(emailInput, passwordInput, passwordConfirmationInput, nameInput, codeInput, tosInput) {
		let email = emailInput.value,
			password = passwordInput.value,
			passwordConfirmed = (passwordConfirmationInput.value === password),
			name = nameInput.value,
			code = codeInput.value,
			tosAccepted = tosInput.checked;

		let errors = "";

		if (name === "") {
			errors += "Name is required\n";
		}

		if (email === "") {
			errors += "Email is required\n";
		} else if (emailInput.checkValidity) {
			if (!emailInput.checkValidity()) {
				errors += "Invalid email\n";
			}
		}

		if (password === "") {
			errors += "Password is required\n";
		} else if (password.length < 6) {
			errors += "Password must be at least 6 characters long\n";
		} else if (!passwordConfirmed) {
			errors += "Passwords do not match\n";
		}

		if (code === "") {
			errors += "Registration Code is required\n"
		}

		if (!tosAccepted) {
			errors += "You must accept the Terms and Conditions to register\n";
		}

		this.setState({
			errors,
		});
		if (errors) {
			return;
		}

		let response = await post("/api/register", {
			email,
			password,
			name,
			code,
			tos: `I, ${name}, accept the terms and conditions of this site.`,
		});

		response = await response.json();

		if (response.result === true) {
			// Attempt Login
			let response = await post("/api/login", { email, password });
			let result = await response.text();

			if (result === "Login Successful") {
				this.props.dispatch({
					type: "view:set",
					data: {
						view: "gameList"
					}
				});
			} else {
				this.setState({
					errors: "Registration succeeded but automatic login attempt failed!\nIf you cannot login, contact the administrator.",
				});
			}
		} else {
			this.setState({
				errors: response.message,
			})
		}
	}

	/**
	 * Render the component
	 * @param {Object} props
	 * @returns {XML}
	 */
	render(props) {
		let emailInput,
			passwordInput,
			passwordConfirmationInput,
			nameInput,
			codeInput,
			tosInput;

		return (
			<div class="registration-form">
				<h2>Registration</h2>

				<p>{registerInfo}</p>

				<input type="text" placeholder="Name" ref={(elm) => { nameInput = elm }} />
				<input type="email" placeholder="Email" ref={(elm) => { emailInput = elm }} />
				<input type="password" placeholder="Password" ref={(elm) => { passwordInput = elm }} />
				<input type="password" placeholder="Confirm Password" ref={(elm) => { passwordConfirmationInput = elm }} />
				<input type="text" placeholder="Registration Code" ref={(elm) => { codeInput = elm }} />
				I accept the <a href="/tos">terms of service</a> of this site <input type="checkbox" ref={(elm) => { tosInput = elm }} />

				<br />
				<span class="error">{this.state.errors}</span>
				<button onClick={
					() => { this.registerCallback(emailInput, passwordInput, passwordConfirmationInput, nameInput, codeInput, tosInput) }
				}>Register</button>
			</div>
		)
	}
}

export default Registration;