import { h, Component } from "preact";
import { connect } from "preact-redux";
import { get, preventDefaultLeftClick } from "../../common/util";

import deepEqual from "deep-equal";

import Home from "./Home";
import LoginBar from "./LoginBar";
import Viewer from "./Viewer";
import Registration from "./Registration";
import GameManagement from "./GameManagement";
import GameEditSelect from "./GameEditSelect";
import AddGame from "./AddGame";

import * as Strings from "../strings";

/**
 * Main app component
 * @extends Component
 */
class App extends Component {
	constructor() {
		super();

		this.updateInfo = this.updateInfo.bind(this);
	}

	uriToView(uri) {
		// Remove leading /
		uri = uri.slice(1);

		if (uri === "") {
			return false;
		}

		uri = uri.split('/');

		switch (uri[0]) {
			case "game":
				return {
					view: "viewer",
					gameId: parseInt(uri[1]),
				};

			case "register":
				return {
					view: "registration"
				};

			case "edit":
				return {
					view: "game_manager",
					id: parseInt(uri[1]),
				};

			case "select-game":
				return {
					view: "select_editable_game"
				};

			case "add-game":
				return {
					view: "add_game",
				};

			case "tos":
				return {
					view: "tos",
				}
		}

		return {
			view: "404"
		};
	}

	static viewToUri(view) {
		switch (view.view) {
			case "viewer":
				let urlTitle = "";
				let titleParts = view.info.name.split('');
				for (let char of titleParts) {
					if (/[a-zA-Z0-9\-_]/.test(char)) {
						urlTitle += char;
					}

					if (char === " ") {
						urlTitle += '_';
					}
				}

				return `/game/${view.gameId}/${urlTitle}`;

			case "registration":
				return "/register";

			case "game_manager":
				return `/edit/${view.id}`;

			case "select_editable_game":
				return "/select-game";

			case "add_game":
				return "/add-game";

			case "tos":
				return "/tos";

			default: //case "gameList":
				return "/";
		}
	}

	componentDidMount() {
		window.onpopstate = history.onpushstate = (e) => {
			this.props.dispatch({
				type: "view:set",
				data: {...e.state, suppressHistory: true},
			})
		}
	}

	async updateInfo() {
		let response, data;

		// User Data
		response = await get("/api/user_details");
		data = await response.json();

		if (!deepEqual(this.props.store.user, data)) {
			this.props.dispatch({
				type: "user:set",
				data,
			});
		}

		// Game List
		response = await get("/api/games");
		data = await response.json();

		if (!deepEqual(this.props.store.games, data)) {
			this.props.dispatch({
				type: "games:set",
				data,
			});
		}
	}

	/**
	 * Renders the component
	 * @param {Object} props
	 * @param {Object} state
	 * @returns {JSX.Element}
	 */
	render(props, state) {
		this.updateInfo();
		let content = null;

		if (state.lastView === undefined) {
			let view = this.uriToView(window.location.pathname);

			view = view ? view : {view: "home"};

			this.setState({
				lastView: view,
			});

			props.dispatch({
				type: "view:set",
				data: view,
			});

			// Restart render with state
			return <div />;
		}

		if (!props.store.view.suppressHistory && !deepEqual(state.lastView, props.store.view)) {
			let uri = App.viewToUri(props.store.view);
			history.pushState(props.store.view, "", uri);
			ga("set", "page", uri);
			ga("send", "pageview");

			this.setState({
				lastView: props.store.view
			});
		}

		document.getElementsByTagName("title")[0].text = "Game Showcase";

		switch (props.store.view.view) {
			case "home":
				content = <Home dispatch={props.dispatch} store={props.store} games={props.store.games}/>;
				break;

			case "viewer":
				content = <Viewer store={props.store} dispatch={props.dispatch} id={props.store.view.gameId} />;
				break;

			case "registration":
				content = <Registration dispatch={props.dispatch} />;
				break;

			case "organisation":
				content = <OrgManagement id={props.store.view.id} dispatch={props.dispatch} />;
				break;

			case "game_manager":
				content = <GameManagement id={props.store.view.id} dispatch={props.dispatch} store={props.store}/>;
				break;

			case "select_editable_game":
				content = <GameEditSelect store={props.store} dispatch={props.dispatch}/>;
				break;

			case "add_game":
				content = <AddGame store={props.store} dispatch={props.dispatch}/>;
				break;

			case "tos":
				content = <section class="lf-wrap">{Strings.tos}</section>;
				break;

			case "404":
				content = <div class="err">
					404 - Resource not found <br />
					<span class="back-link" onClick={() => { window.history.back() }}>Go back</span>
				</div>;
				break;
		}


		return (
			<div class="container">
				<a class="no-a" href="/" onClick={preventDefaultLeftClick}><h1 onClick={() => {this.props.dispatch({type: "view:set", data: {view: "home"}})}}>SSA Game Showcase</h1></a>
				<LoginBar dispatch={props.dispatch} updateState={props.updateState} details={props.store.user}/>
				{ content }
			</div>
		)
	}
}

export default connect(state => ({store: state}))(App);