import { h, render, Component } from "preact";
import { get, post, onEnter } from "../../common/util";

/**
 * LoginBar component
 * @extends Component
 */
class LoginBar extends Component {
	constructor() {
		super();
		this.state = {
			is_signing_in: false,
			login_reponse: "",
		};
	}

	/**
	 * Render the logged out view
	 * @returns {XML}
	 */
	renderLoggedOut() {
		return (
			<div class="login-bar">
				<a class="button" onClick={() => {this.props.dispatch({
					type: "view:set",
					data: {
						view: "registration"
					}
				})}}>Register</a>
				<span class="button" onClick={() => {this.setState({is_signing_in: true})}}>Sign In</span>
			</div>
		);
	}

	/**
	 * Log the user out
	 * @returns {Promise.<void>}
	 */
	async logout() {
		let response = await get("/api/logout");
		if (response.ok) {
			this.props.updateState();
		} else {
			//Todo, proper error system
			alert("Error signing out!");
		}
	}

	/** Attempt to sign in
	 * @param {HTMLInputElement} emailEntry
	 * @param {HTMLInputElement} passwordEntry
	 * @returns {Promise.<void>}
	 */
	async signIn(emailEntry, passwordEntry) {
		// Basic checks
		if (!emailEntry.value || !passwordEntry.checkValidity()) {
			this.setState({login_reponse: "Please enter a valid email address"});
			return;
		}

		if (!passwordEntry.value) {
			this.setState({login_reponse: "Please enter a password"});
			return;
		}

		// Attempt login

		let response = await post("/api/login", { email: emailEntry.value, password: passwordEntry.value });
		let result = await response.text();

		if (result === "Login Successful") {
			this.props.updateState();
			this.setState({login_reponse: "", is_signing_in: false});
		} else {
			this.setState({login_reponse: "Login failed"});
		}
	}

	/**
	 * Render the logged in view
	 * @param {{name: string, organisation: string}} user
	 * @returns {XML}
	 */
	renderLoggedIn(user) {
		// get user organisational role

		let orgManagementButton = null;
		if (user.role === "admin") {
			//orgManagementButton = <span class="button">Organisation Management</span>
		}

		let adminButton = null;
		if (user.is_admin) {
			adminButton = <span class="button">Admin</span>;
		}

		return (
			<div class="login-bar logged-in">
				<div class="user-info">
					<span class="user-name">{user.name}</span>
					<span class="user-org">{user.organisation}</span>
				</div>

				<div>

					<span class="button" onClick={
						() => {this.props.dispatch({
							type: "view:set",
							data: {
								view:"select_editable_game"}
							}
						);}
					}>Game Management</span>
					{orgManagementButton}
					{adminButton}
					<span class="button" onClick={this.logout.bind(this)}>Sign Out</span>
				</div>
			</div>
		)
	}

	/**
	 * Render the signing in view
	 * @returns {XML}
	 */
	renderSigningIn() {
		let emailEntry = null;
		let passwordEntry = null;

		let signInCallback = () => {this.signIn.bind(this)(emailEntry, passwordEntry)};
		let keypressSignInCallback = onEnter(signInCallback);

		return (
			<div class="login-bar">
				<span class="error">{this.state.login_reponse}</span>

				<input type="email" autofocus ref={(input) => {emailEntry = input}} onKeyDown={keypressSignInCallback} placeholder="Email"/>

				<input type="password" ref={(input) => {passwordEntry = input}} onKeyDown={keypressSignInCallback} placeholder="Password"/>

				<span class="button" onClick={signInCallback}>Sign In</span>
				<span class="button" onClick={() => {
					emailEntry.value = "";
					passwordEntry.value = "";
					this.setState({is_signing_in: false, login_reponse:""})
				}}>Cancel</span>
			</div>
		);
	}

	/**
	 * Render the component
	 * @param {Object} props
	 * @param {Object} state
	 * @returns {XML}
	 */
	render(props, state) {
		let details = props.details;

		if (!state.is_signing_in) {
			if (details.signed_in) {
				return this.renderLoggedIn.bind(this)(details.user);
			} else {
				return this.renderLoggedOut.bind(this)();
			}
		} else {
			return this.renderSigningIn.bind(this)();
		}
	}
}

export default LoginBar;