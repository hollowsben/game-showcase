import { h, Component } from "preact"
import { post, get } from "../../common/util";
import deepEqual from "deep-equal";

class UserGameRelationships extends Component {
	constructor() {
		super();

		this.state = {
			org_users: [],
			relationships: [],
		};

		this.getInfo = this.getInfo.bind(this);
		this.renderRow = this.renderRow.bind(this);
		this.renderAddUser = this.renderAddUser.bind(this);
		this.removeUser = this.removeUser.bind(this);
		this.addUser = this.addUser.bind(this);
	}

	async getInfo() {
		let relationships_response = await get(`/api/users/game/${this.props.game_id}`);
		let relationships;
		if (!relationships_response.ok) {
			relationships = [];
		} else {
			relationships = await relationships_response.json();
		}

		let org_users_response = await get(`/api/users/org`);
		let org_users;
		if (!org_users_response.ok) {
			org_users = [];
		} else {
			org_users = await org_users_response.json();
		}

		let can_edit;

		if (this.props.store.user.user.is_admin) {
			can_edit = true;
		} else {
			can_edit = (this.props.store.user.user.role === "admin");
		}

		if (!deepEqual(org_users, this.state.org_users) || !deepEqual(relationships, this.state.relationships)) {
			this.setState({
				org_users,
				relationships,
				can_edit,
			})
		}
	}

	async removeUser(user_id) {
		let response = await post("/api/users/game/remove", {
			user_id,
			game_id: this.props.game_id,
		});

		if (response.ok) {
			this.getInfo();
		} else {
			//todo communicate error
		}
	}

	async addUser() {
		if (!this.state.new_user_input) {
			return;
		}

		let user_id = parseInt(this.state.new_user_input.value);

		let response = await post("/api/users/game/add", {
			user_id,
			game_id: this.props.game_id,
		});

		if (response.ok) {
			this.getInfo();
		} else {
			//todo communicate error
		}
	}

	renderRow(relationship) {
		if (this.state.can_edit) {
			return (
				<tr>
					<td>{relationship.name}</td>
					<td>{relationship.role}</td>
					<td><span class="remove-user" onClick={() => {this.removeUser(relationship.user_id)}}>Remove</span></td>
				</tr>
			);
		} else {
			return (
				<tr>
					<td>{relationship.name}</td>
					<td>{relationship.role}</td>
					<td />
				</tr>
			);
		}
	}

	renderAddUser() {
		if (!this.state.can_edit) {
			return null;
		}

		let availableUsers = this.state.org_users.filter((user) => {
			for (let relationship of this.state.relationships) {
				if (relationship.user_id === user.id) {
					return false;
				}
			}

			return true;
		});

		let options = availableUsers.map((user) => {
			return <option value={user.id}>{user.name}</option>;
		});

		return(
			<div>
				<select ref={(elm) => {
					this.state.new_user_input = elm;
				}}>
					{options}
				</select>
				<button onClick={this.addUser}>Add User</button>
			</div>
		)
	}


	render() {
		this.getInfo();

		return (
			<div class="user-game-relationships">
				<span class="input-description">Assigned Users</span>
				<table class="relationship-list">
					<tr>
						<th>Name</th>
						<th>Org Role</th>
						<th />
					</tr>
					{this.state.relationships.map(this.renderRow)}
				</table>
				<br />
				{this.renderAddUser()}
			</div>
		);
	}
}

export default UserGameRelationships;