import { h , Component } from "preact";
import { post, get, Progress } from "../../common/util";
import UserGameRelationships from "./UserGameRelationships";
import deepEqual from "deep-equal";

class GameManagement extends Component {
	constructor() {
		super();

		this.state = {
			components: {},
			uploadInfo: "",
			infoInfo: "",
		};

		this.getGameInfo = this.getGameInfo.bind(this);
		this.renderPublicOption = this.renderPublicOption.bind(this);
		this.uploadFile = this.uploadFile.bind(this);
		this.uploadProgress = this.uploadProgress.bind(this);
		this.updateInfo = this.updateInfo.bind(this);
		this.setVisibility = this.setVisibility.bind(this);
	}

	async getGameInfo() {
		let info = await get(`/api/game/${this.props.id}/info`);
		let data = await info.json();

		if (!deepEqual(this.state.info, data)) {
			this.setState({
				info: data,
			})
		}
	}

	uploadProgress(loaded, total, progress, speed) {
		this.setState({
			uploadInfo: `${Progress.bytesToString(loaded)} / ${Progress.bytesToString(total)} (${(progress * 100).toFixed(0)}%) ${Progress.bytesToString(speed)}/s`
		});
	}

	async uploadFile() {
		let fileInput = this.state.components.fileInput;

		if (fileInput.files.length === 0) {
			//no files specified for upload
			return;
		}

		let request = await get(`/api/game/${this.state.info.id}/file/upload`);

		if (!request.ok) {
			//error
			return;
		}

		let upload_data = await request.json();

		let form = new FormData();

		for (let key in upload_data.fields) {
			form.append(key, upload_data.fields[key]);
		}

		let progress = new Progress(this.uploadProgress);

		let response = await new Promise((resolve) => {
			let request = new XMLHttpRequest();


			request.upload.onprogress = progress.update;

			request.onreadystatechange = () => {
				if (request.readyState === XMLHttpRequest.DONE) {
					resolve(request);
				}
			};

			request.upload.onload = () => {
				this.setState({
					uploadInfo: "Upload Complete",
				})
			};

			request.upload.onerror = () => {
				this.setState({
					uploadInfo: "Upload Failed",
				})
			};

			request.upload.onabort = () => {
				this.setState({
					uploadInfo: "Upload Aborted",
				})
			};

			form.append("file", fileInput.files[0]);
			request.open("POST", upload_data.url, true);
			request.send(form);
		});
	}

	async updateInfo() {
		this.setState({
			infoInfo: "Updating..."
		});

		let response = await post(`/api/game/${this.state.info.id}/info/update`, {
			name: this.state.components.nameInput.value,
			description: this.state.components.descriptionInput.value,
			authors: this.state.components.authorsInput.value,
		});

		if (response.ok) {
			this.setState({
				infoInfo: "Updated Successfully"
			});
			this.getGameInfo();
		} else {
			this.setState({
				infoInfo: "Failed to Update"
			})
		}
	}

	renderPublicOption() {
		if (this.state.info.is_public) {
			return ([
				<span>
					Game is currently <span class="publicity-status public">Public</span>
					 - Your game is playable by anyone!</span>,
				<br />,
				<span
					onclick={() => {this.setVisibility(0)}}
					class="publicity-status change">Change to private. Your game will be playable by members of your organisation only.</span>
			])
		} else {
			return ([
				<span>
					Game is currently <span class="publicity-status private">Private</span>
					 - Your game is playable by members of your organisation only.</span>,
				<br />,
				<span
					onclick={() => {this.setVisibility(1)}}
					class="publicity-status change">Change to public. Your game will be playable by anyone!</span>
			])
		}
	}

	async setVisibility(set_public) {
		let response = await post(`/api/game/${this.state.info.id}/set-privacy`, {
			set_public
		});

		if (response.ok) {
			this.getGameInfo();
		}
	}

	render() {
		this.getGameInfo();

		if (!this.props.store.user.user) {
			return <div class="err">Access Denied - Please log in</div>
		}

		if (!this.state.info) {
			return <span>Loading</span>;
		}

		return (
			<div class="game-manager">
				<h2>Editing: {this.state.info.name}</h2>

				<UserGameRelationships game_id={this.state.info.id} store={this.props.store}/>

				<hr />

				<span class="input-description">Title</span>
				<input ref={(elm) => { this.state.components.nameInput = elm }} value={this.state.info.name}/>

				<span class="input-description">Authors</span>
				<p class="help">This will display below the title, no special formatting required</p>
				<input ref={(elm) => { this.state.components.authorsInput = elm }} value={this.state.info.authors}/>

				<span class="input-description">Description</span>
				<textarea ref={(elm) => { this.state.components.descriptionInput = elm }}>{this.state.info.description}</textarea>

				<span class="info-info">{this.state.infoInfo}</span>

				<button onClick={this.updateInfo}>Update</button>

				<hr />

				<span class="input-description">Change game file</span>
				<p class="help">{`Upload your game as a zip file containing all your assests, with an index.html file at the root.
    
Your game must display in the index.html file from the top left corner at a resolution of 576x432px or 100% of the viewport with NO MARGINS.

All links to resources in your zip file must be relative.
External resources are allowed (but must accept CORS requests from https://ssa.hollows.cloud where applicable).`}</p>
				<input ref={(elm) => { this.state.components.fileInput = elm }} type="file" />
				<span class="upload-info">{this.state.uploadInfo}</span>
				<button onClick={this.uploadFile}>Upload</button>

				<hr />

				{this.renderPublicOption()}
			</div>
		)
	}
}

export default GameManagement;
