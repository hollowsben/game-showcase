import { h, Component } from "preact"
import GameList from "./GameList";
import * as Strings from "../strings";

export default class Home extends Component {
	render(props) {
		return (
			<div>
				<div class="section lf-wrap">{Strings.intro}</div>
				<GameList dispatch={props.dispatch} store={props.store} games={props.games}/>
			</div>
		);
	}
}