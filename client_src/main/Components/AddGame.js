import { h , Component } from "preact";
import { post } from "../../common/util";

class AddGame extends Component {
	constructor() {
		super();

		this.state = {
			input: {},
			error: "",
		};

		this.addGame = this.addGame.bind(this);
	}

	async addGame() {
		let title = this.state.input.title.value;
		let description = this.state.input.description.value;

		if (title.length === 0 ) {
			this.setState({
				error: "Title cannot be empty!"
			});
			return;
		}

		let response = await post("/api/game/add", {
			title,
			description,
		});

		if (response.ok) {
			this.props.dispatch({
				type: "view:set",
				data: {
					view: "game_manager",
					id: parseInt(await response.text()),
				}
			})
		} else {
			this.setState({
				error: await response.text(),
			});
		}
	}

	render() {
		if (!this.props.store.user.user) {
			return <div class="err">Access Denied - Please log in</div>
		}

		return (
			<div class="add-game">
				<h2>New Game</h2>

				<span class="input-description">Title</span>
				<input ref={(elm) => {this.state.input.title = elm}} />

				<span class="input-description">Description	</span>
				<textarea ref={(elm) => {this.state.input.description = elm}} />

				<span class="error">{this.state.error}</span>
				<button onClick={this.addGame}>Add</button>
			</div>
		)
	}
}

export default AddGame;
