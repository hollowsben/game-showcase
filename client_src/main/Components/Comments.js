import { h, Component } from "preact";
import { get, post, datetimeToString } from "../../common/util";
import deepEqual from "deep-equal";

/**
 * Comment display component
 * @extends Component
 */
class Comments extends Component {
	constructor() {
		super();
		this.hasUpdated = false;

		this.state = {
			components: {}
		}


		// Bind methods
		this.renderComment = this.renderComment.bind(this);
		this.updateComments = this.updateComments.bind(this);
		this.submitComment = this.submitComment.bind(this);
		this.submitPublicComment = this.submitPublicComment.bind(this);
	}

	/**
	 * Gets comment data from the server
	 * @returns {Promise.<void>}
	 */
	async updateComments() {
		this.hasUpdated = true;

		let response = await get(`/api/comments/${this.props.gameId}`);
		let data = await response.json();

		if (!deepEqual(this.props.store.comments, data)) {
			this.props.dispatch({
				type: "comments:set",
				data,
			});
		}
	}

	/**
	 * Renders a given comment
	 * @param {{name: string, organisation: string, date_added: Date, text: string}} info - Comment data
	 * @returns {XML}
	 */
	renderComment(info) {
		let classes = [];

		if (this.props.store.user.signed_in && info.user_id === this.props.store.user.user.id) {
			classes.push("self");
		}

		let hidden_label = null;
		if (info.visible === 0) {
			classes.push("hidden");
			hidden_label = <span class="hidden-label">[Hidden]</span>;
		}

		let special = null;
		if (info.is_admin) {
			special = <span class="decorate admin" />;
		}

		return <div data-type={info.type} class={"comment " + classes.join(' ')}>
			<div class="user-info">
				<span class="name">{info.name}</span>
				{special}
				<br />
				<span data-org={info.organisation} class="organisation">{info.organisation}</span>
			</div>

			<span class="date">{datetimeToString(info.date_added)}</span>
			{hidden_label}

			<p class="body">{info.text}</p>
		</div>
	}

	async submitComment() {
		this.state.components.error_display.innerText = "";

		let response = await post("/api/comments/submit", {
			gameId: this.props.gameId,
			text: this.state.components.comment_body_input.value,
		});

		if (response.ok) {
			this.state.components.comment_body_input.value = "";
			this.updateComments();
		} else {
			this.state.components.error_display.innerText = await response.text();
		}
	}

	async submitPublicComment() {
		this.state.components.error_display.innerText = "";

		let response = await post("/api/comments/submit", {
			gameId: this.props.gameId,
			text: this.state.components.comment_body_input.value,
			name: this.state.components.name_input.value,
		});

		if (response.ok) {
			this.state.components.comment_body_input.value = "";
			this.updateComments();
		} else {
			this.state.components.error_display.innerText = await response.text();
		}
	}

	/**
	 * Renders the comment submit area
	 * @returns {XML}
	 */
	renderTextbox() {
		if (this.props.store.user.signed_in) {
			return (
				<div class="comment-submit">
					<span class="title">Submit Comment</span>

					<textarea style="" placeholder="Leave your feedback!" ref={(elm) => { this.state.components.comment_body_input = elm }}/>
					<span class="info error" ref={(elm) => { this.state.components.error_display = elm }}/>

					<div>
						<span class="info">Be constructive not demeaning. Abusive comments will be removed and may result in a ban.</span>
						<button onClick={this.submitComment}>Submit</button>
					</div>
				</div>
			);
		} else {
			return (
				<div class="comment-submit">
					<span class="title">Submit Comment</span>

					<span class="input-label">Name: </span><input type="text" ref={(elm) => { this.state.components.name_input = elm }} />
					<textarea style="" placeholder="Leave your feedback!" ref={(elm) => { this.state.components.comment_body_input = elm }}/>
					<span class="info error" ref={(elm) => { this.state.components.error_display = elm }}/>

					<div>
						<span class="info">Be constructive not demeaning. Abusive comments will be removed and may result in a ban.</span>
						<button onClick={this.submitPublicComment}>Submit</button>
					</div>
				</div>
			)
		}
	}

	/**
	 * Renders the component
	 * @param {Object} props
	 * @returns {XML}
	 */
	render(props) {
		this.updateComments();

		return (
			<div class="comments">
				{this.renderTextbox.bind(this)()}
				<div class="comment-list">
					{props.store.comments.map((info) => this.renderComment(info))}
				</div>
			</div>
		)
	}
}

export default Comments;