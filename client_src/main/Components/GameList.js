import { h, Component } from "preact";
import GameCard from "./GameCard";

/**
 * Displays a list of Games
 * @extends Component
 */
class GameList extends Component {
	/**
	 * Renders the component
	 * @param {Object} props
	 * @returns {XML}
	 */
	render(props) {
		return (
			<div class="game-list">
				{props.games.map((info) => <GameCard dispatch={props.dispatch} store={props.store} info={info}/>)}
			</div>
		)
	}
}

export default GameList;