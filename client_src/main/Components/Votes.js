import { h, Component } from "preact";
import { get, post } from "../../common/util";
import deepEqual from "deep-equal";

/**
 * Star rating component
 * @extends Component
 */
class Votes extends Component {
	constructor() {
		super();
		this.state = {
			data: {
				avg_rating: 0,
				num: 0,
				user_rating: 0,
			},
			user_selection: 0,
		};

		this.updateVotes = this.updateVotes.bind(this);
		this.renderStar = this.renderStar.bind(this);
	}

	/**
	 * Gets vote data from the server
	 * @returns {Promise.<void>}
	 */
	async updateVotes() {
		let response = await get(`/api/votes/${this.props.gameId}`);
		let data = await response.json();

		if (!deepEqual(this.state.data, data)) {
			this.setState({
				data,
				user_selection: data.user_rating,
			})
		}
	}

	/**
	 * Sets the user selection to the specified value
	 * @param value
	 */
	setUserSelection(value) {
		// Do not show change in ui if user is not logged in
		if (!this.props.store.user.signed_in) {
			return;
		}

		if (this.props.store.user_selection !== value) {
			this.setState({
				user_selection: value,
			})
		}
	}

	/**
	 * Pushes new user vote to server
 	 * @param {int} value - Vote value
	 */
	async setUserVote(value) {
		if (!this.props.store.user.signed_in) {
			return;
		}

		let response = await post("/api/votes/set", {
			gameId: this.props.gameId,
			value,
		});

		if (response.ok) {
			this.updateVotes();
		}
	}

	/**
	 * Renders an individual star
	 * @param {number} i - Which star this is in the sequence
	 * @param {number} value - Proportion of star to fill, clamped to [0, 1]
	 * @param {boolean} selected - Whether of not the star has been selected by the user
	 * @returns {XML}
	 */
	renderStar(i, value, selected) {
		let starInnerHTML = (
			`<g id="Canvas" transform="translate(3 0)">
				<clipPath id="clip-0" clip-rule="evenodd">
					<path d="M -3 0L 97 0L 97 100L -3 100L -3 0Z" fill="#FFFFFF"/>
				</clipPath>
				<g id="V3" clip-path="url(#clip-0)">
					<g id="base">
						<use xlink:href="#path0_fill" transform="translate(-3 5)" fill="#C4C4C4"/>
					</g>
					<g id="fill">
						<use xlink:href="#path0_fill" transform="translate(-3 5)" fill="#F2C94C"/>
					</g>
					<g id="selected-border">
						<use xlink:href="#path1_fill" transform="translate(-3.40601 2)" fill="#F2994A"/>
					</g>
				</g>
			</g>
			<defs>
				<path id="path0_fill" d="M 50 0L 61.2257 34.5491L 97.5528 34.5491L 68.1636 55.9017L 79.3893 90.4509L 50 69.0983L 20.6107 90.4509L 31.8364 55.9017L 2.44717 34.5491L 38.7743 34.5491L 50 0Z"/>
				<path id="path1_fill" fill-rule="evenodd" d="M 62.244 36.7064L 50.406 0L 38.568 36.7064L 0 36.6221L 31.2517 59.2236L 19.2534 95.8779L 50.406 73.14L 81.5586 95.8779L 69.5603 59.2236L 100.812 36.6221L 62.244 36.7064ZM 56.6924 44.3475L 50.406 25L 44.1196 44.3475L 23.7764 44.3475L 40.2344 56.305L 33.948 75.6525L 50.406 63.695L 66.864 75.6525L 60.5776 56.305L 77.0356 44.3475L 56.6924 44.3475Z"/>
			</defs>`
		);

		if (value >= 0.5) {
			value = 1;
		} else {
			value = 0;
		}

		let classes = "star";

		if (selected) {
			classes += " selected";
		}

		if (value) {
			classes += " filled";
		}

		if (this.props.store.user.signed_in) {
			classes += " selectable";
		}

		return (
			<svg class={classes} onMouseEnter={() => this.setUserSelection.bind(this)(i + 1)}  onClick={() => this.setUserVote.call(this, i+1)} width="100" height="100" viewBox="0 0 100 100" dangerouslySetInnerHTML={{__html: starInnerHTML}} />
		);
	}

	/**
	 * Render the component
	 * @returns {XML}
	 */
	render() {
		this.updateVotes();
		let stars = [];

		for (let i = 0; i < 5; i++) {
			stars.push(this.renderStar(i, this.state.data.avg_rating - i, (i < this.state.user_selection)));
		}

		return (
			<div class="votes" onMouseLeave={() => {
				this.setState({
					user_selection: this.state.data.user_rating,
				});
			}}>
				{stars}
			</div>
		)
	}
}

export default Votes;