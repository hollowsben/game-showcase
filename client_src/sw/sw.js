/**
 * @namespace sw
 */

/**
 * Log an error
 * @param {string} s - Error message
 * @memberOf sw
 */
function error(s) {
	console.error(`SERVICE WORKER: ${s}`);
}

/**
 * Log a message
 * @param {string} s - Message
 * @memberOf sw
 */
function log(s) {
	console.log(`SERVICE WORKER: ${s}`);
}


// Wrap in self executing function to ensure import is above hoisted functions
(function() {

	/**
	 * Log install event
	 * @method
	 * @name event:install
	 * @listens serviceWorker:install
	 * @memberOf sw
	 */
	self.addEventListener("install", function (event) {
		log("Installed")
	});

	/**
	 * Log activate event
	 * @method
	 * @name event:activate
	 * @listens serviceWorker:activate
	 * @memberOf sw
	 */
	self.addEventListener("activate", function (event) {
		log("Activated")
	});

	/**
	 * Handle message from client
	 * @method
	 * @name event:message
	 * @listens serviceWorker:message
	 * @memberOf sw
	 */
	self.addEventListener("message", async function (event) {
		let message = event.data;
		let response = false;

		switch(message.type) {
			case "load":
				response = loadFiles(message.data);
		}

		event.ports[0].postMessage(response);
	});

	/**
	 * Intercept fetch requests from client and server from loaded files if able
	 * @method
	 * @name event:fetch
	 * @listens serviceWorker:fetch
	 * @memberOf sw
	 */
	self.addEventListener("fetch", function(event) {
		let response = null;
		let localPath = getLocalPath(event.request.url);

		if (localPath !== null) {
			let file = getLoadedFile(localPath);

			response = new Response(file.data, {
				headers: {
					"Content-Type": "application/text",//file.mimeType,
					"Cache-Control": "no-cache, no-store, must-revalidate",
				}
			});
		} else {
			response = fetch(event.request);
		}


		event.respondWith(response);
	});

	/**
	 * Returns local path of uri
	 * @param {string} path - URI
	 * @returns {?string} - Local path
	 * @memberOf sw
	 */
	function getLocalPath(path) {
		path = decodeURIComponent(path);

		if (gameFiles === null) {
			return null;
		}

		for (let file in gameFiles) {
			if (self.registration.scope + file === path) {
				return file;
			}
		}

		return null;
	}

	/**
	 * Returns a file from loaded cache
	 * @param {string} path
	 * @returns {ArrayBuffer}
	 * @memberOf sw
	 */
	function getLoadedFile(path) {
		return gameFiles[path];
	}


	let gameFiles = null;

	/**
	 * Load files into cache
	 * @param {Object} files
	 * @returns {boolean}
	 * @memberOf sw
	 */
	function loadFiles(files) {
		gameFiles = files;
		return true;
	}
})();