/**
 * @namespace viewer
 */

import { get, Progress } from "../common/util";

zip.workerScriptsPath = "/lib/";

let preventLoad = false;

// Raven setup
(function logging() {
	let wrapMethod = function(console, level, callback) {
		let originalConsoleLevel = console[level];
		let originalConsole = console;

		if (!(level in console)) {
			return;
		}

		let sentryLevel = level === 'warn' ? 'warning' : level;

		console[level] = function() {
			let args = [].slice.call(arguments);

			let msg = '' + args.join(' ');
			let data = {level: sentryLevel, logger: 'console', extra: {arguments: args}};

			if (level === 'assert') {
				if (args[0] === false) {
					// Default browsers message
					msg = 'Assertion failed: ' + (args.slice(1).join(' ') || 'console.assert');
					data.extra.arguments = args.slice(1);
					callback && callback(msg, data);
				}
			} else {
				callback && callback(msg, data);
			}

			// this fails for some browsers. :(
			if (originalConsoleLevel) {
				// IE9 doesn't allow calling apply on console functions directly
				// See: https://stackoverflow.com/questions/5472938/does-ie9-support-console-log-and-is-it-a-real-function#answer-5473193
				Function.prototype.apply.call(originalConsoleLevel, originalConsole, args);
			}
		};
	};

	let console = window.console;

	let logLevels = ['debug', 'info', 'warn', 'error'];
	if ('assert' in console) {
		logLevels.push('assert');
	}

	let callback = function (msg, data) {
		Raven.captureMessage(msg, data);
	};

	let level = logLevels.pop();
	while (level) {
		wrapMethod(console, level, callback);
		level = logLevels.pop();
	}
})();


// Check if serviceWorker is registered
if (!navigator.serviceWorker.controller) {
	error("Awaiting ServiceWorker install...");
	preventLoad = true;
	navigator.serviceWorker.ready.then(() => {
		setTimeout(() => window.location.reload(), 500);
	});
}

function error(text) {
	document.getElementsByTagName("main")[0].classList.add("show-error");
	document.getElementById("error").textContent = text;
}

function set_status(text) {
	document.getElementById("progress-text").textContent = text;
}

function set_progress(amount) {
	let percentage = amount * 100;
	document.getElementById("bar").style.width = percentage + '%';
}


/**
 * Log a message
 * @param {string} s - Message
 * @memberOf viewer
 */
function log(s) {
	console.log(`VIEWER: ${s}`);

}

if ("serviceWorker" in navigator) {
	window.addEventListener('load', function() {
		navigator.serviceWorker.register('sw.js').then(function(registration) {
			// Registration was successful
			log('ServiceWorker registration successful with scope: ' + registration.scope);
		}).catch(function(err) {
			// registration failed :(
			error('ServiceWorker registration failed: ' + err);
		});
	});
}

/**
 * Send a message to the service worker
 * @param {string} type
 * @param {*} data
 * @returns {Promise}
 * @memberOf viewer
 */
function SW_send(type, data) {
	return new Promise(function(resolve, reject) {
		// Create a Message Channel
		let msg_chan = new MessageChannel();

		// Handler for recieving message reply from service worker
		msg_chan.port1.onmessage = function(event) {
			if (event.data.error ){
				reject(event.data.error);
			} else {
				resolve(event.data);
			}
		};

		// Send message to service worker along with port for reply
		navigator.serviceWorker.controller.postMessage({type:type, data:data}, [msg_chan.port2]);
	});
}



window.onload = async function() {
	if(!compatibilityCheck() || preventLoad) {
		return false;
	}

	window.onhashchange = function() {
		window.location.reload();
	};

	clear();

	if (window.location.hash !== "") {
		let game_id = window.location.hash.slice(1);

		let game_info = await get(`/api/game/${game_id}/info`);

		if (game_info.status !== 200) {
			error("Could not retrieve game info");
			return;
		}

		game_info = await game_info.json();

		Raven.captureBreadcrumb({
			message: `Loading Game: ${game_id}, ${game_info.name}, v${game_info.version}`,
			data: game_info,
			category: "page_init",
			level: "info",
		});

		document.getElementById("title").textContent = game_info.name;
		document.getElementsByTagName("title")[0].text = game_info.name + " | Borderless Viewer | Game Showcase";

		let status = await prepFiles(game_id);

		if (status) {
			await loadGame();
		}
	} else {
		error("No game specified");
	}
};

/**
 * Checks if the browser supports service workers, logging errors
 * @returns {boolean}
 * @memberOf viewer
 */
function compatibilityCheck() {
	if (!navigator.serviceWorker) {
		error("Your browser does not support Service Workers\nPlease use Chrome, Firefox, or Opera to view this content");
		return false;
	}

	return true
}

/**
 * Load the game
 * @returns {Promise.<void>}
 * @memberOf viewer
 */
async function loadGame() {
	set_status("Downloading game index file");
	// Load html file
	let stream_gameIndexHTML = await fetch("index.html");
	let gameIndexHTML = await stream_gameIndexHTML.text();

	set_status("Injecting game");

	// Pause for dramatic effect
	setTimeout(() => {
		document.open();
		document.write(gameIndexHTML);
		document.close();
	}, 2000);
}

/**
 * Download and extract files for specified game
 * @param {int} gameId
 * @returns {Promise.<boolean>} - Whether files were loaded successfully
 * @memberOf viewer
 */
async function prepFiles(gameId) {
	set_status("Downloading Files");

	let blob = await downloadGameBlob(gameId);

	if (!blob || blob.size === 0) {
		error("Failed to download game files");
		return false;
	}

	set_status("Extracting Files");
	let files = await extractZip(blob);

	if (!files) {
		return false;
	}

	set_status("Sending Files to Service Worker");
	let response = await SW_send("load", files);

	if (!response) {
		error("Failed to send files to service worker");
		return false;
	}
	return true;
}

/**
 * Updates progress UI component with download progress
 * @memberOf viewer
 */
function updateDownloadProgress(loaded, total, percentage, speed) {
	set_progress(percentage);

	let info = `${Progress.bytesToString(loaded)} / ${Progress.bytesToString(total)} (${(percentage * 100).toFixed(0)}%) ${Progress.bytesToString(speed)}/s`;

	set_status(`Downloading\n${info}`);
}

/**
 * Downloads the game files from the server
 * @param {int} id - Game id
 * @returns {Promise.<*>}
 * @memberOf viewer
 */
async function downloadGameBlob(id) {
	let response = await new Promise((resolve) => {
		let request = new XMLHttpRequest();
		let progress = new Progress(updateDownloadProgress);

		request.onprogress = progress.update;
		request.onreadystatechange = () => {
			if (request.readyState === XMLHttpRequest.DONE) {
				resolve(request);
			}
		};
	
		request.open("GET", `/api/game/${id}/file`, true);
		request.responseType = "blob";
		request.send();

	});

	if (response.status !== 200) {
		return false;
	}

	return response.response;
}

/**
 * Extracts files from zip file
 * @param {ArrayBuffer} blob - Zipped files
 * @returns {Promise.<{}>} - Extracted files
 * @memberOf viewer
 */
async function extractZip(blob) {
	let reader = await new Promise((resolve, reject) => {
		zip.createReader(new zip.BlobReader(blob), (reader) => {
			resolve(reader);
		});
	});

	let entries = await new Promise((resolve) => {
		reader.getEntries((entries) => {
			resolve(entries);
		});
	});

	let files = {};

	// Test for empty zip
	if (entries.length === 0) {
		error("Game files are empty!");
		return false;
	}

	let length = 0;

	// Get metadata
	for (let entry of entries) {
		// Ignore directories
		if (!entry.directory) {
			length++;
			files[entry.filename] = {
				size: entry.uncompressedSize,
				data: null,
				mimeType: zip.getMimeType(entry.filename),
			}
		}
	}

	if (!files.hasOwnProperty("index.html")) {
		error("Game files are invalid!\nNo index found");
		return false;
	}

	// Extract file data
	set_progress(0);
	let i = 1;
	for (let entry of entries) {
		// Ignore directories
		if (!entry.directory) {
			set_status("Decompressing " + entry.filename);

			files[entry.filename].data = await new Promise((resolve) => {
				entry.getData(new zip.BlobWriter(), (blob) => { resolve(blob) })
			});

			set_progress(i / length);
			i++;
		}
	}

	return files;
}

function clear() {
	localStorage.clear();
	sessionStorage.clear();
}
