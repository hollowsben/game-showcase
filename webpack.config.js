const webpack = require('webpack');
const path = require('path');

// Configs

let main = {
	entry: {
		main: __dirname + "/client_src/main/index.js",
		viewer: __dirname + "/client_src/viewer/index.js",
	},
	output: {
		path: __dirname + "/client/js/",
		filename: '[name].js'
	},
	devtool: 'source-map',
	module: {
		rules: [
			{test: /\.json$/, loader: "json"},
			{test: /\.jsx?$/, loader: "babel-loader"},
		]
	},
};

let service_worker = {
	entry: __dirname + "/client_src/sw/sw.js",
	output: {
		path: __dirname + "/client/viewer/",
		filename: 'sw.js'
	},
	devtool: 'source-map',
	module: {
		rules: [
			{test: /\.json$/, loader: "json"},
			{test: /\.jsx?$/, loader: "babel-loader"},
		]
	},
};

module.exports = [main, service_worker];
