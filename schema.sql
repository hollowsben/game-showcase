
use game_showcase;

create table leaderboard (
	id int auto_increment,
	ip text not null,
	name text not null,
	score numeric not null,
	game_id int null,

	constraint leaderboard_pk primary key (id)
);
create index leaderboard_game_id_index on leaderboard (game_id);

create table organisation_roles (
	user_id int not null,
	organisation_id int not null,
	role text not null,

	constraint organisation_roles_pk
	    primary key (user_id, organisation_id)
);

create table games (
    id int not null auto_increment,
    name text not null,
    authors text,
    description text,
    version int default 0,
    date_updated TIMESTAMP not null default current_timestamp on update current_timestamp,

    is_public boolean not null default false,
    organisation_id int not null,

    leaderboard_secret text,

    constraint games_pk primary key (id)
);

create table users_to_game_info (
    user_id int not null,
    game_id int not null,

    constraint users_to_game_info_pk primary key (user_id, game_id)
);

create table organisations (
    id int not null auto_increment,
    name text not null,

    constraint organisations_pk primary key (id)
);

create table users (
    id int not null auto_increment,
    name text not null,
    email text not null,
    password_hash text not null,
    organisation_id int not null,
    is_admin boolean not null default false,

    constraint users_pk primary key (id)
);

create table votes (
    game_id int not null,
    user_id int not null,
    rating int not null,

    constraint votes_pk primary key (game_id, user_id)
);


create table registration_codes (
    code VARCHAR(30) not null,
    active boolean not null default false,
    organisation_id int not null,
    initial_role text not null,

    constraint registration_codes_pk primary key (code)
);

create table log (
    id int not null auto_increment,
    level text not null,
    `system` text not null,
    type text not null,
    value text not null,

    constraint log_pk primary key (id)
);

create table comments (
    id int not null auto_increment,
    user_id int not null,
    game_id int not null,
    text text not null,
    visible boolean not null default true,
    date_added TIMESTAMP default current_timestamp,

    constraint comments_pk primary key (id)
);
create index comments_game_id_index on comments (game_id);


create table public_comments (
    id int not null auto_increment,
    ip text not null,
    name text not null,
    game_id int not null,
    text text not null,
    visible boolean not null default true,
    date_added TIMESTAMP default current_timestamp,

    constraint public_comments_pl primary key (id)
);
create index public_comments_game_id_index on public_comments (game_id);

create table blacklist (
    id int not null auto_increment,
    ip VARCHAR(39) not null,

    constraint blacklist_pk primary key (id)
);
create index blacklist_ip_index on blacklist (ip);

create table game_files (
    game_id int not null,
    file blob,

    constraint game_files_pk primary key (game_id)
);

DELIMITER //

create procedure get_comments(
    in game_id_in int,
    in is_admin_in boolean
)
begin
    SELECT
        u.name as name,
        o.name as organisation,
        date_added,
        text,
        visible,
        is_admin
    FROM comments
    INNER JOIN users u on comments.user_id = u.id
    INNER JOIN organisations o on u.organisation_id = o.id
    WHERE
          game_id = game_id_in AND
          (visible = true OR is_admin_in = true)
    UNION
    SELECT
        name,
        '' as organisation,
        date_added,
        text,
        visible,
        false as is_admin
    FROM public_comments
    WHERE
          game_id = game_id_in AND
          (visible = true OR is_admin_in = true)
    ORDER BY date_added DESC;
end //

create procedure get_ip_frequency(
    in ip_in text
)
begin
    SELECT
        'minute' as period,
        (SELECT
            COUNT(id) FROM public_comments
            WHERE ip = ip_in AND
                  date_added >= NOW() - INTERVAL 1 MINUTE
        ) as count
    UNION
    SELECT
        '10minutes' as period,
        (SELECT
            COUNT(id) FROM public_comments
            WHERE ip = ip_in AND
                  date_added >= NOW() - INTERVAL 10 MINUTE
        ) as count
    UNION
    SELECT
        '30minutes' as period,
        (SELECT
            COUNT(id) FROM public_comments
            WHERE ip = ip_in AND
                  date_added >= NOW() - INTERVAL 30 MINUTE
        ) as count
    UNION
    SELECT
        'hour' as period,
        (SELECT
            COUNT(id) FROM public_comments
            WHERE ip = ip_in AND
                  date_added >= NOW() - INTERVAL 1 HOUR
        ) as count
    ;
end //

create procedure get_user_info (
    in user_id_in int
)
begin
    SELECT id, is_admin, users.organisation_id as organisation_id, name, email, role
    FROM users
    INNER JOIN organisation_roles o on users.id = o.user_id
    WHERE id = user_id_in;
end //

DELIMITER ;
