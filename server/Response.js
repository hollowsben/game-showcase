/**
 * @namespace Response
 */

/**
 * Return a successful response object
 *
 * Parameters can be as follows
 *     - (code, message)
 *     - (code)
 *     - (message)
 *     - ()
 * Missing values will be automatically set
 *
 * @param {int|string} p1
 * @param {?string} p2
 * @returns {{success: {true}, code: int, message: string}}
 *
 * @memberOf Response
 */
function ok(p1, p2) {
	let code, message;

	if (!p1 && !p2) {
		code = 200;
		message = defaultMessage[200];
	} else if (!p2) {
		if (typeof(p1) === "number") {
			code = p1;
			message = defaultMessage[code];
		} else {
			code = 200;
			message = p1;
		}
	} else {
		code = p1;
		message = p2;
	}

	return {
		success: true,
		code,
		message,
	}
}

/**
 * Return an unsuccessful response object
 * @param {int} code
 * @param {string} customMessage
 * @returns {{success: {false}, code: int, message: string}}
 *
 * @memberOf Response
 */
function fail(code, customMessage = false) {
	return {
		success: false,
		code,
		message: (customMessage) ? customMessage : defaultMessage[code],
	}
}

/**
 * Lookup table of default messages
 * @type {Object.<int, string>}
 *
 * @memberOf Response
 */
let defaultMessage = {
	200: "Request Successful",
	201: "Object creation successful",

	301: "Moved Permanently",
	303: "See Other",

	400: "Bad Request",
	401: "Unauthorised - Please log in and retry request",
	403: "You do not have permission to access this resource",
	404: "Resource not found",
	409: "Request rejected due to conflict",
	410: "Resource is no longer available",
	413: "Request payload too large",
	423: "Resource has been locked by an administrator",
	429: "Too many requests - Please wait before performing another request",

	500: "Internal Server Error",
	503: "Service unavailable",
};

/**
 * Common Responses
 * @type {Object.<string, Object.<int, string>>}
 *
 * @memberOf Response
 */
let	common = {
	db_failure: fail(503, "Database query failed"),
	db_invalid: fail(502, "Database returned invalid data"),
	no_login: fail(401),
	no_access: fail(403),
	bad_request: fail(400),
	incomplete: fail(400, "Required information is missing from request"),
	not_found: fail(404),
};

/**
 * Koa middleware to add helper function to ctx
 * @returns {function({ctx}, {next})}
 *
 * @memberOf Response
 */
function middleware() {
	return (async(ctx, next) => {
		ctx.doResponse = function(response) {
			ctx.status = response.code;
			ctx.body = response.message;
		};

		await next();
	});
}

/**
 * Response class and helper functions
 * @module Response
 *
 * @memberOf Response
 */
module.exports = {
	defaultMessage,
	common,
	ok,
	fail,
	middleware,
};