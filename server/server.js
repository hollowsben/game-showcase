#!/usr/bin/env node

/**
 * Main server code
 * @module Server
 */

const config = require("./config.json");

const fs = require("fs");
const mime = require("mime");

const Koa = require("koa");
const BodyParser = require("koa-bodyparser");
const Router = require("koa-router");

const AWS = require("aws-sdk");

const StaticLoader = require("./staticLoader");
const DatabaseConnection = require("./database");
const Session = require("./session");
const Data = require("./data");
const Action = require("./action");

const Response = require("./Response");
const Level = require("./Levels");

const app = new Koa();
const router = new Router({
	prefix: "/api"
});
const staticLoader = new StaticLoader();

(async function() {
	// Initial Setup
	await staticLoader.load("client");
	let db = new DatabaseConnection(config.database);

	db.addPool("default", {connectionLimit: 5});
	db.addPool("file", {connectionLimit: 10});

	let data = new Data(db);
	let action = new Action(db);

	AWS.config.update({
		accessKeyId: config.aws.key,
		secretAccessKey: config.aws.secret,
		region: config.aws.region,
	});

	let s3 = new AWS.S3();

	// Routing and middleware

	router.get("/games", async (ctx) => {
		ctx.body = await data.getGamesList(ctx.session);
	});

	router.get("/games/editable", async (ctx) => {
		let [list, error] = await data.getEditableGames(ctx.session);

		if (error) {
			ctx.doResponse(error);
		} else {
			ctx.body = list;
		}
	});

	router.post("/game/add", async (ctx) => {
		if (!ctx.session.user || !ctx.session.user.id) {
			ctx.doResponse(Response.common.no_login);
		}

		if (!ctx.request.body || !ctx.request.body.title || !ctx.request.body.description) {
			ctx.doResponse(Response.common.bad_request);
		}

		if (ctx.request.body.title.length === 0) {
			ctx.doResponse(Response.fail(400, "Title cannot be empty!"));
		}

		let response = await action.addGame(ctx.request.body.title, ctx.request.body.description, 0, ctx.session);
		ctx.doResponse(response);
	});

	router.get("/game/:id/info", async (ctx) => {
		if (!ctx.params || !ctx.params.id) {
			ctx.doResponse(Response.common.bad_request);
			return;
		}

		ctx.body = await data.getGameInfo(ctx.params.id, ctx.session);
	});

	router.post("/game/:id/set-privacy", async (ctx) => {
		if (!ctx.params || !ctx.params.id) {
			ctx.doResponse(Response.common.bad_request);
			return;
		}

		if (!ctx.session.user || !ctx.session.user.id) {
			ctx.doResponse(Response.common.no_login);
			return;
		}

		if (!ctx.request.body || !ctx.request.body.hasOwnProperty("set_public")) {
			ctx.doResponse(Response.common.bad_request);
			return;
		}

		ctx.doResponse(await action.setGameVisibility(ctx.params.id, ctx.request.body.set_public, ctx.session));
	});

	router.post("/game/:id/info/update", async (ctx) => {
		if (!ctx.params || !ctx.params.id) {
			ctx.doResponse(Response.common.bad_request);
			return;
		}

		let req = ctx.request.body;

		if (!req.name || !req.description || !req.authors) {
			ctx.doResponse(Response.common.bad_request);
			return;
		}

		let result = await action.updateGameInfo(ctx.params.id, req.name, req.description, req.authors, ctx.session);

		if (result) {
			ctx.doResponse(Response.ok());
		} else {
			ctx.doResponse(Response.common.db_failure);
		}
	});

	router.get("/game/:id/file/upload", async (ctx) => {
		if (!ctx.params || !ctx.params.id) {
			ctx.doResponse(Response.common.bad_request);
			return;
		}

		if (!ctx.session.user) {
			ctx.doResponse(Response.common.no_login);
			return;
		}

		if (!ctx.session.user.is_admin) {
			let org = ctx.session.user.organisation_id;
			let role = await data.getOrgRole(ctx.session.user.id, org);

			if (role !== "admin") {
				let [allowedUsers, error] = await data.getGameUsers(ctx.params.id, ctx.session);
				if (error) {
					ctx.doResponse(error);
					return;
				}

				let ok = false;
				for (let user of allowedUsers) {
					if (user.user_id === ctx.session.user.id) {
						ok = true;
						break;
					}
				}

				if (!ok) {
					ctx.doResponse(Response.common.no_access);
					return;
				}
			}
		}

		let version = await action.incrementVersion(ctx.params.id);

		await s3.deleteObject({
			Bucket: config.aws.bucket,
			Key: `${ctx.params.id}/file.v${version - 1}`
		});

		let params = {
			Bucket: config.aws.bucket,
			Expires: 300,
			Fields: {
				key: `${ctx.params.id}/file.v${version}`
			},
			Conditions: [
				["content-length-range", 1, 838860800] // Max 50MB, disallow 0B upload (probable error)
			]
		};

		let upload_data = await s3.createPresignedPost(params);

		if (!data) {
			ctx.doResponse(Response.fail(500, "S3 signing failure"));
			return;
		}

		ctx.body = upload_data;
	});

	router.get("/game/:id/file", async(ctx) => {
		if (!ctx.params || !ctx.params.id) {
			ctx.doResponse(Response.common.bad_request);
			return;
		}

		let gameInfo = await data.getGameInfo(ctx.params.id, ctx.session);

		let url;
		if (gameInfo.is_public || ctx.session.user.is_admin) {
			url = `${config.aws.cdn}/${ctx.params.id}/file.v${gameInfo.version}`;
		} else if (ctx.session.user.organisation_id === gameInfo.organisation_id) {
			url = `${config.aws.cdn}/${ctx.params.id}/file.v${gameInfo.version}`; //todo, change when perms added

		} else {
			ctx.doResponse(Response.common.no_access);
			return;
		}

		ctx.response.status = 303;
		ctx.response.set("Location", url);
	});

	router.get("/comments/:id", async(ctx) => {
		if (!ctx.params || !ctx.params.id) {
			ctx.doResponse(Response.common.bad_request);
			return;
		}

		ctx.body = await data.getComments(ctx.params.id, ctx.session);
	});

	router.get("/votes/:id", async(ctx) => {
		if (!ctx.params || !ctx.params.id) {
			ctx.doResponse(Response.common.bad_request);
		}

		ctx.body = await data.getVotes(ctx.params.id, ctx.session);
	});

	router.post("/votes/set", async(ctx) => {
		if(!ctx.session || !ctx.session.user) {
			ctx.doResponse(Response.common.no_login);
			return;
		}

		let req = ctx.request.body;

		if (!req.gameId || !req.value) {
			ctx.doResponse(Response.common.bad_request);
			return;
		}

		let result = await action.setVote(req.gameId, req.value, ctx.session);

		ctx.doResponse(Response.ok());
	});

	router.post("/comments/submit", async(ctx) => {
		if (!ctx.session || !ctx.session.user) {
			let req = ctx.request.body;

			if (!req.gameId) {
				ctx.doResponse(Response.common.bad_request);
				return;
			}

			if (!req.text) {
				ctx.doResponse(Response.fail(400, "Comment body cannot be empty"));
				return;
			}

			if (!req.name) {
				ctx.doResponse(Response.fail(400, "Name cannot be empty"));
				return;
			}

			if (req.name.length > 30) {
				ctx.doResponse(Response.fail(400, "Name cannot be longer than 30 characters"));
				return;
			}

			let response = await action.postPublicComment(req.gameId, req.name, ctx.request.header["cf-connecting-ip"], req.text, ctx.session);
			ctx.doResponse(response);
		} else {
			let req = ctx.request.body;

			if (!req.gameId) {
				ctx.doResponse(Response.common.bad_request);
				return;
			}

			if (!req.text) {
				ctx.doResponse(Response.fail(400, "Comment body cannot be empty"));
				return;
			}

			let response = await action.postComment(req.gameId, req.text, ctx.session);
			ctx.doResponse(response);
		}
	});

	router.get("/user_details", async (ctx) => {
		if (!ctx.session.user) {
			ctx.body = { signed_in: false }
		} else {
			ctx.body = {
				signed_in: true,
				user: ctx.session.user
			};
		}
	});

	router.post("/login", async (ctx) => {
		let session = ctx.session;

		let req = ctx.request.body;

		if (req && req.email && req.password) {
			let id = await data.testPassword(req.email, req.password);

			if (id) {
				session.user = { id };
				session.user = await data.getUserInfo(id, ctx.session);

				action.log(Level.INFO, "Login", "Successful", `${id}, ${ctx.request.header["cf-connecting-ip"]}`);
				ctx.body = "Login Successful";
			} else {
				action.log(Level.INFO, "Login", "Failed", `${req.email}, ${ctx.request.header["cf-connecting-ip"]}`);
				ctx.body = "Login Failed";
			}
		} else {
			ctx.doResponse(Response.common.not_found);
		}
	});

	router.get("/logout", async (ctx) => {
		Session.destroy(ctx.session.id);

		if (ctx.request.query.redirect) {
			ctx.response.status = 303;
			ctx.response.set("Location", ctx.request.origin + ctx.request.query.redirect)
		}

		ctx.body = "Logout Complete";
	});

	router.post("/register", async (ctx) => {
		if (
			!ctx.request.body ||
			!ctx.request.body.email ||
			!ctx.request.body.password ||
			!ctx.request.body.name ||
			!ctx.request.body.code ||
			!ctx.request.body.tos
		) {
			ctx.doResponse(Response.common.bad_request);
			return;
		}

		let [result, code, part, message] = await action.register(
			ctx.request.body.email,
			ctx.request.body.password,
			ctx.request.body.name,
			ctx.request.body.code,
			ctx.request.body.tos
		);

		if (!result) {
			ctx.type = "json";
			ctx.doResponse(Response.fail(code, JSON.stringify({
				result,
				part,
				message
			})));
			return;
		}


		action.log(Level.INFO, "Registration", "Complete", ctx.request.header["cf-connecting-ip"]);

		ctx.body = {
			result,
		}
	});

	router.get("/users/org", async (ctx) => {
		// Get current user org
		if (!ctx.session || !ctx.session.user) {
			ctx.doResponse(Response.common.no_login);
			return;
		}

		let [list, error] = await data.getOrganisationUsers(ctx.session.user.organisation_id, ctx.session);
		if (error) {
			ctx.doResponse(error);
		} else {
			ctx.body = list;
		}
	});

	router.get("/users/game/:id", async (ctx) => {
		if (!ctx.params || !ctx.params.id) {
			ctx.doResponse(Response.common.bad_request);
			return;
		}

		let [list, error] = await data.getGameUsers(ctx.params.id, ctx.session);
		if (error) {
			ctx.doResponse(error);
		} else {
			ctx.body = list;
		}
	});

	router.post("/users/game/remove", async (ctx) => {
		if (!ctx.request.body || !ctx.request.body.user_id || !ctx.request.body.game_id) {
			ctx.doResponse(Response.common.bad_request);
			return;
		}

		let response = await action.unassignUserFromGame(ctx.request.body.user_id, ctx.request.body.game_id, ctx.session);
		ctx.doResponse(response);
	});

	router.post("/users/game/add", async (ctx) => {
		if (!ctx.request.body || !ctx.request.body.user_id || !ctx.request.body.game_id) {
			ctx.doResponse(Response.common.bad_request);
			return;
		}

		let response = await action.assignUserToGame(ctx.request.body.user_id, ctx.request.body.game_id, ctx.session);
		ctx.doResponse(response);
	});

	router.get("/leaderboard/:id", async (ctx) => {
		if (!ctx.params || !ctx.params.id) {
			ctx.doResponse(Response.common.bad_request);
			return;
		}

		let leaderboard;
		if (ctx.request.query && ctx.request.query.limit) {
			leaderboard = await data.getLeaderboard(ctx.params.id, parseInt(ctx.request.query.limit));
		} else {
			leaderboard = await data.getLeaderboard(ctx.params.id);
		}

		ctx.body = leaderboard;
	});

	router.post("/leaderboard/:id/add", async (ctx) => {
		if (!ctx.params || !ctx.params.id) {
			ctx.doResponse(Response.common.bad_request);
			return;
		}

		if (!ctx.request.body || !ctx.request.body.name || !ctx.request.body.score || !ctx.request.body.hash) {
			ctx.doResponse(Response.common.bad_request);
			return;
		}

		ctx.body = action.addToLeaderboard(ctx.params.id, ctx.request.body, ctx.request.header["cf-connecting-ip"]);
	});

	// Letsencrypt validation
	app.use(async (ctx, next) => {
		if (!ctx.url.startsWith("/.well-known/")) {
			await next();
			return;
		}

		if (ctx.url.indexOf("..") !== -1) {
			// Deny backtracking to ensure security
			await next();
			return;
		}

		// Remove leading /
		let url = ctx.url.slice(1);

		// No absolute paths
		if (url[0] === '/') {
			await next();
			return;
		}


		let file = null;
		try {
			file = await new Promise((resolve, reject) => {
				fs.readFile(url, (err, data) => {
					if (err) {
						reject(err);
					}

					resolve(data);
				});
			});
		} catch (e) {
			await next();
			return;
		}

		if (file) {
			ctx.response.set("Content-Type", mime.getType(url));
			ctx.body = file;
			return;
		}

		await next();
	});

	app.use(Response.middleware());
	app.use(BodyParser());
	app.use(Session.middleware());
	app.use(router.middleware());
	app.use(staticLoader.middleware());

	// Default
	app.use((ctx) => {
		// Ignore files in viewer scope
		if (ctx.url.split('/')[1] === "viewer") {
			ctx.doResponse(Response.common.not_found);
			return;
		}

		//Ignore api

		if (ctx.url.startsWith("/api")) {
			ctx.doResponse(Response.common.not_found);
			return;
		}

		// Else let js route
		let index = staticLoader.getFile("/index.html");
		ctx.response.set("Content-Type", index.mime);
		ctx.body = index.body;
	});

	// Start Server
	let listenOn = config.server.port;

	// If user has supplied a custom port via arguments
	if (process.argv.length > 2) {
		let argListenOn = process.argv[2];

		if (!isNaN(argListenOn)) {
			argListenOn = +argListenOn; // To number
		}

		listenOn = argListenOn;
	}

	app.listen(listenOn);
	console.log(`Listening on ${listenOn}`);
})();
