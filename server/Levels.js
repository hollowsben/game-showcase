module.exports =  {
	INFO: 0,
	GAME_ERROR: 1,
	WARN: 2,
	ERROR: 3,
	CRITICAL: 4,
};