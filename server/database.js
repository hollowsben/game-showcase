const mysql = require("mysql");

/**
 * Handles database connection and queries
 */
class DatabaseConnection {
	/**
	 * Create a database connection
	 * @constructor
	 * @param {Object} defaultConfig - Default configuration to use when adding a pool
	 */
	constructor(defaultConfig = {}) {
		this.pools = {};
		this.defaultConfig = defaultConfig;
	}

	/**
	 * Creates a new connection pool
	 * @param {string} key - Name to assign to pool
	 * @param {Object} config - Configuration to use for pool. Values in this overwrite those in default config
	 */
	addPool(key, config = {}) {
		this.pools[key] = mysql.createPool(
			Object.assign({}, this.defaultConfig, config)
			);
	}

	/**
	 * Execute a query on the database
	 * @param {string} pool - Name of connection pool to use
	 * @param {string} statement - SQL statement to execute
	 * @param {Array} values - Values to insert into '?' in statement. Will be sanitised
	 * @returns {Promise.<Object|Error>}
	 */
	query(pool, statement, values = []) {
		return new Promise((resolve, reject) => {
			this.pools[pool].query(statement, values, (err, res, fields) => {
				if (err) {
					reject(err);
				}

				resolve(res);
			});
		});
	}
}

module.exports = DatabaseConnection;