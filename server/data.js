const bcrypt = require("bcryptjs");
const Response = require("./Response");

/**
 * Class containing function to retrieve data from the database
 */
class Data {
	/**
	 * Create a Data instance and link to Database
	 * @param {DatabaseConnection} db - DatabaseConnection to use when retrieving data
	 */
	constructor(db) {
		this.db = db;
	}


	async getLeaderboard(game_id, limit=10) {
		return await this.db.query("default", "SELECT name, score FROM leaderboard WHERE game_id=? ORDER BY score DESC LIMIT ?", [game_id, limit]);
	}

	/**
	 * Returns the role of the given user in the given organisation
	 * @param user_id
	 * @param organisation_id
	 * @returns {Promise.<string>}
	 */
	async getOrgRole(user_id, organisation_id) {
		let r = await this.db.query("default", "SELECT role FROM organisation_roles WHERE user_id=? AND organisation_ID=?", [user_id, organisation_id]);

		if (!r) {
			return null;
		}

		return r[0].role;
	}

	/**
	 * Get information about the requested user
	 * @param {int} id - ID of the user
	 * @param {Object} session
	 * @returns {Promise.<Object>}
	 */
	async getUserInfo(id, session) {
		if (session.user && (id === session.user.id || session.user.is_admin)) {
			return (await this.db.query("default", "CALL get_user_info(?)", [id]))[0][0]; // [results[0], meta]
		} else {
			return false;
		}
	}

	async getEditableGames(session) {
		if (!session.user || !session.user.id) {
			return [null, Response.common.no_login];
		}

		let r;
		if (session.user.is_admin) {
			r = await this.db.query("default", "SELECT id, name, is_public FROM games");
		} else if (session.user.role === "admin") {
			r = await this.db.query("default", "SELECT id, name, is_public FROM games WHERE organisation_id = ?", [session.user.organisation_id]);
		} else {
			r = await this.db.query("default", "SELECT id, name, is_public FROM games INNER JOIN users_to_game_info ON users_to_game_info.game_id = games.id WHERE users_to_game_info.user_id = ?", [session.user.id]);
		}

		if (!r) {
			return [null, Response.common.db_failure];
		}

		return [r, null];
	}

	/**
	 * Returns list of users and their roles in an organisation
	 * @param organisation_id
	 * @param session
	 * @returns {Promise.<[]>} Form of [user_list, Response.error]
	 */
	async getOrganisationUsers(organisation_id, session) {
		if (!session.user || !session.user.id) {
			return [null, Response.common.no_login];
		}

		// This is only needed by admins
		// Only allow if global or organisation admin
		if (!session.user.is_admin) {
			let r1 = await this.db.query("default", "SELECT role FROM organisation_roles WHERE user_id = ?", [session.user.id]);
			if (r1[0].role !== "admin") {
				return [null, Response.common.no_access];
			}
		}

		let r2 = await this.db.query("default", "SELECT users.id, users.name, role FROM organisations INNER JOIN users ON users.organisation_id = organisations.id INNER JOIN organisation_roles ON users.id = organisation_roles.user_id WHERE organisations.id=?", [organisation_id]);
		if (!r2) {
			return [null, Response.common.db_failure];
		}

		return [r2, null];
	}

	/**
	 * Returns list of users assigned to a game and their roles in an organisation
	 * @param game_id
	 * @param session
	 * @returns {Promise.<[]>} Form of [user_list, Response.error]
	 */
	async getGameUsers(game_id, session) {
		if (!session.user || !session.user.id) {
			return [null, Response.common.no_login];
		}

		let gameInfo = await this.getGameInfo(game_id, session);

		if (!session.user.is_admin && session.user.organisation_id !== gameInfo.organisation_id) {
			return [null, Response.common.no_access];
		}

		let r2 = await this.db.query("default", "SELECT users.id AS user_id, name, role FROM users_to_game_info INNER JOIN users ON users.id = users_to_game_info.user_id INNER JOIN organisation_roles ON users.id = organisation_roles.user_id WHERE game_id = ?", [game_id]);
		if (!r2) {
			return [null, Response.common.db_failure];
		}

		return [r2, null];
	}

	/**
	 * Returns id if password matches user record, null if it does not.
	 * @param {string} email
	 * @param {string} password
	 * @returns {Promise.<int|null>}
	 */
	async testPassword(email, password) {
		let r = (await this.db.query("default", "SELECT id, password_hash FROM users WHERE email = ?", [email]))[0];

		if (!r) {
			// No user
			// Prevent timing attack by running compare operation

			await bcrypt.compare('a', 'b');
			return null;
		}

		if (await bcrypt.compare(password, r.password_hash)) {
			return r.id;
		} else {
			return null;
		}
	}

	async getGameInfo(game_id, session) {
		let r = [];

		//if (session.user) {
			r = await this.db.query("default", "SELECT games.id as id, games.name as name, authors, description, date_updated, is_public, version, organisations.name as organisation, organisations.id as organisation_id FROM games INNER JOIN organisations on games.organisation_id = organisations.id where games.id = ?", [game_id]);
		//}

		return r[0];
	}

	/**
	 * Returns a list of games the user has access to
	 * @param {Object} session
	 * @returns {Promise.<Array>}
	 */
	async getGamesList(session) {
		let r = [];

		if (session.user) {
			if (session.user.is_admin) {
				r = await this.db.query("default", "SELECT games.id as id, games.authors as authors, games.name as name, description, date_updated, is_public, organisations.name as organisation_name FROM games INNER JOIN organisations ON games.organisation_id = organisations.id ORDER BY date_updated DESC");
			} else {
				r = await this.db.query("default", "SELECT games.id as id, games.authors as authors, games.name as name, description, date_updated, is_public, organisations.name as organisation_name FROM games INNER JOIN organisations ON games.organisation_id = organisations.id where (organisation_id=? OR is_public=1) ORDER BY date_updated DESC", [session.user.organisation_id]);
			}
		} else {
			r = await this.db.query("default", "SELECT games.id as id, games.authors as authors, games.name as name, description, date_updated, is_public, organisations.name as organisation_name FROM games INNER JOIN organisations ON games.organisation_id = organisations.id WHERE is_public=1 ORDER BY date_updated DESC");
		}

		return r;
	}

	/**
	 * Returns a list of comments the user has access to for a specified game
	 * @param {int} game_id
	 * @param {Object} session
	 * @returns {Promise.<Array>}
	 */
	async getComments(game_id, session) {
		let r = [];
		let is_admin = (session.user && session.user.is_admin);

		r = await this.db.query("default", "CALL get_comments(?, ?)", [game_id, is_admin]);
		return r[0];	// r is of form [results, meta]
	}

	/**
	 * Returns the average vote for a game
	 * @param {int} game_id
	 * @param {Object} session
	 * @returns {Promise.<number>}
	 */
	async getVotes(game_id, session) {
		let r = null;

		if (session.user) {
			r = await this.db.query("default", "SELECT (SELECT AVG(rating) FROM votes WHERE game_id=?) as avg_rating, (SELECT COUNT(rating) FROM votes WHERE game_id=?) as num, (SELECT rating FROM votes WHERE game_id=? AND user_id=?) as user_rating;", [game_id, game_id, game_id, session.user.id])
		} else {
			r = await this.db.query("default", "SELECT (SELECT AVG(rating) FROM votes WHERE game_id=?) as avg_rating, (SELECT COUNT(rating) FROM votes WHERE game_id=?) as num;", [game_id, game_id]);
		}

		return r[0];
	}
}

module.exports = Data;
