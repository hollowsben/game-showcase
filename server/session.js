const uuid = require("uuid/v4");

/*
TODO

Add ip check to session
Add expiry
 */

/**
 * Max time without a request for a session to expire (in hours)
 * @const
 * @private
 * @type {number}
 */

const SESSION_EXPIRY_HOURS = 4;
/**
 * How often to check for expired sessions (in minutes)
 * @const
 * @private
 * @type {number}
 */
const SESSION_EXPIRY_CHECK_MIN = 10;


/**
 * Max time without a request for a session to expire (in milliseconds)
 * @const
 * @private
 * @type {number}
 */
const SESSION_EXPIRY_MS = SESSION_EXPIRY_HOURS * 3600000;

/**
 * How often to check for expired sessions (in milliseconds)
 * @const
 * @private
 * @type {number}
 */
const SESSION_EXPIRY_CHECK_MS = SESSION_EXPIRY_CHECK_MIN * 60000;

/**
 * Session information store
 * @private
 * @type {{}}
 */
var SessionStore = {};

/**
 * Class for managing sessions
 */
class Session {
	/**
	 * Koa middleware to provide session to ctx
	 * @returns {function({ctx}, {next})}
	 */
	static middleware() {
		// Setup expiry
		setInterval(Session.expire, SESSION_EXPIRY_CHECK_MS);

		return (async (ctx, next) => {
			let session_id = ctx.cookies.get("session_id");
			let session = undefined;

			if (!session_id || !SessionStore.hasOwnProperty(session_id)) {
				session = new Session();
				SessionStore[session.id] = session;
				ctx.cookies.set("session_id", session.id, {overwrite: true});
			} else {
				session = SessionStore[session_id];

				// Update session activity date
				session.last_activity = new Date();
			}

			ctx.session = session;
			await next();
		})
	}

	/**
	 * Removes expired sessions
	 */
	static expire() {
		let curDate = new Date();
		for (let session_id in SessionStore) {
			let session = SessionStore[session_id];

			if (curDate - session.last_activity > SESSION_EXPIRY_MS) {
				Session.destroy(session_id);
			}
		}
	}


	/**
	 * Removes session with the given id
	 * @param {string} id
	 */
	static destroy(id) {
		delete SessionStore[id];
	}

	/**
	 * Creates a new session, or connects with an existing one
	 * @constructor
	 * @param {?string} id - Existing client id, or null to create a new session
	 */
	constructor(id) {
		if (!id) {
			id = uuid();
			while(SessionStore.hasOwnProperty(id)) {
				id = uuid();
			}
		}

		this.id = id;
		this.last_activity = new Date();
	}
}

module.exports = Session;