const bcrypt = require("bcryptjs");
const Response = require("./Response");
const Level = require("./Levels");
const RSA = require("node-rsa");
const ProfanityFilter = require("bad-words");
const crypto = require("crypto");

/**
 * Class containing functions to perform actions on the database
 */
class Action {
	/**
	 * Create an Action instance and link to Database
	 * @constructor
	 * @param {DatabaseConnection} db - DatabaseConnection to use when performing actions
	 */
	constructor(db) {
		this.db = db;

		this.checkBlacklist = this.checkBlacklist.bind(this);
		this.postPublicComment = this.postPublicComment.bind(this);
		this.addToLeaderboard = this.addToLeaderboard.bind(this);
	}

	/**
	 * Register a user
	 * @param {string} email
	 * @param {string} password
	 * @param {string} name
	 * @param {string} code - Registration code
	 * @param {boolean} tos - Whether TOS has been accepted
	 * @returns {Promise.<[]>} - Whether registration was successful
	 */
	async register(email, password, name, code, tos) {
		// Validate TOS
		if (tos !== `I, ${name}, accept the terms and conditions of this site.`) {
			return [false, 403, "tos", "TOS not accepted"];
		}

		// Validate password length
		if (password.length < 6) {
			return [false, 403, "password", "Password is less than 6 characters in length"];
		}

		// Ensure email has not already been registered
		let email_result = await this.db.query("default", "SELECT count(*) as user_exists FROM users WHERE email=?", [email]);
		if (!email_result) {
			return [false, 503, "general", "Database query failed"];
		}
		if (email_result[0].user_exists) {
			return [false, 409, "email", "This email address has already been registered"];
		}

		// Validate registration code
		let reg_code_result = await this.db.query("default", "SELECT active, organisation_id, initial_role FROM registration_codes WHERE code=?", [code]);

		if (!reg_code_result) {
			return [false, 503, "general", "Database query failed"];
		}
		if (reg_code_result.length === 0) {
			return [false, 403, "code", "Invalid registration code"];
		}

		reg_code_result = reg_code_result[0];

		// All checks passed, register user
		let password_hash = await bcrypt.hash(password, 10);

		// User record
		let r3 = await this.db.query("default", "INSERT INTO users (email, password_hash, name, organisation_id) VALUES (?, ?, ?, ?)", [email, password_hash, name, reg_code_result.organisation_id]);
		if (!r3) {
			return [false, 503, "general", "Database query failed"];
		}

		let userId = r3.insertId;
		if (!userId) {
			return [false, 500, "general", "Registration failed due to internal error"];
		}

		// Link user to organisation
		let r4 = await this.db.query("default", "INSERT INTO organisation_roles (user_id, organisation_id, role) VALUES (?, ?, ?)", [userId, reg_code_result.organisation_id, reg_code_result.initial_role]);
		if (!r4 || !r4.affectedRows) {
			return [false, 500, "general", "Registration succeeded but could not assign organisation role. Contact the administrator."];
		}

		// Registration successful
		return [true];
	}

	/**
	 * Grant a user access to modify a game
	 * @param user_id
	 * @param game_id
	 * @param session
	 * @returns {Promise.<{}>} - Response to send to client
	 */
	async assignUserToGame(user_id, game_id, session) {
		if (!session.user || !session.user.id) {
			return Response.common.no_login;
		}

		// Only allow if global or organisation admin
		if (!session.user.is_admin) {
			let r1 = await this.db.query("default", "SELECT role FROM organisation_roles INNER JOIN games ON organisation_roles.organisation_id = games.organisation_id WHERE user_id = ? AND games.id = ?", [session.user.id, game_id]);

			if (r1[0].role !== "admin") {
				return Response.common.no_access;
			}
		}

		let r2 = await this.db.query("default", "INSERT INTO users_to_game_info (user_id, game_id) VALUES (?, ?)", [user_id, game_id]);

		if (!r2 || !r2.affectedRows) {
			return Response.common.db_failure;
		}

		return Response.ok();
	}

	log(level, system, type, value) {
		this.db.query("default", "INSERT INTO log (level, system, type, value) VALUES (?, ?, ?, ?)", [level, system, type, value]);
	}

	async setGameVisibility(game_id, is_public, session) {
		if (!session.user || !session.user.id) {
			return Response.common.no_login;
		}

		let r;

		if (session.user.is_admin) {
			r = await this.db.query("default", "UPDATE games SET is_public=? WHERE id=?", [is_public, game_id]);
		} else {
			r = await this.db.query("default", "UPDATE users INNER JOIN users_to_game_info on users.id = users_to_game_info.user_id INNER JOIN games on users_to_game_info.game_id = games.id SET games.is_public=? WHERE games.id = ? AND users.id = ?", [is_public, game_id, session.user.id]);
		}

		if (r.affectedRows > 0) {
			return Response.ok();
		} else {
			return Response.fail(423, "Operation failed");
		}
	}

	/**
	 * Revoke a user's access to modify a game
	 * @param user_id
	 * @param game_id
	 * @param session
	 * @returns {Promise.<{}>} - Response to send to client
	 */
	async unassignUserFromGame(user_id, game_id, session) {
		if (!session.user || !session.user.id) {
			return Response.common.no_login;
		}

		// Only allow if global or organisation admin
		if (!session.user.is_admin) {
			let r1 = await this.db.query("default", "SELECT role FROM organisation_roles INNER JOIN games ON organisation_roles.organisation_id = games.organisation_id WHERE user_id = ? AND games.id = ?", [session.user.id, game_id]);

			if (r1[0].role !== "admin") {
				return Response.common.no_access;
			}
		}

		let r2 = await this.db.query("default", "DELETE FROM users_to_game_info WHERE user_id=? AND game_id=?", [user_id, game_id]);
		if (!r2 || !r2.affectedRows) {
			return Response.common.db_failure;
		}

		return Response.ok();
	}

	/**
	 * Post a comment
	 * @param {int} game_id
	 * @param {string} text
	 * @param {Object} session
	 * @returns {Promise.<Response>}
	 */
	async postComment(game_id, text, session) {
		if (!session.user || !session.user.id) {
			return Response.common.no_login;
		}

		let filter = new ProfanityFilter();
		if (filter.isProfane(text)) {
			return Response.fail(400, "Profanity is not permitted.");
		}

		let r = await this.db.query("default", "INSERT INTO comments (user_id, game_id, text) VALUES (?, ?, ?)",
			[session.user.id, game_id, text]);

		if (!r || !r.insertId) {
			return Response.common.db_failure;
		}

		return Response.ok();
	}

	async postPublicComment(game_id, name, ip, text, session) {
		let [not_in_blacklist, response] = await this.checkBlacklist(ip);
		if (!not_in_blacklist) {
			return response;
		}

		let [rate_check_ok, response2] = await this.checkRate(ip);
		if (!rate_check_ok) {
			return response2;
		}


		let filter = new ProfanityFilter();
		if (filter.isProfane(text)) {
			return Response.fail(400, "Profanity is not permitted.");
		}

		let r = await this.db.query("default", "INSERT INTO public_comments (ip, game_id, name, text) VALUES (?, ?, ?, ?)",
			[ip, game_id, name, text]);

		if (!r || !r.insertId) {
			return Response.common.db_failure;
		}

		return Response.ok();
	}

	async checkBlacklist(ip) {
		let r = await this.db.query("default", "select count(id) as in_blacklist FROM blacklist WHERE ip=?", [ip]);
		if (!r) {
			return [false, Response.common.db_failure];
		}

		if (r[0].in_blacklist) {
			return [false, Response.fail(403, "Your IP address has been blacklisted")]
		}

		return [true, null];
	}

	async checkRate(ip) {
		let r = await this.db.query("default", "CALL get_ip_frequency(?)", [ip]);
		if (!r) {
			return [false, Response.common.db_failure];
		}

		for (let row of r[0]) {
			if (row.period === "minute" && row.count > 3) {
				return [false, Response.fail(429, "Rate limit exceeded")]
			}

			if (row.period === "10minutes" && row.count > 10) {
				return [false, Response.fail(429, "Rate limit exceeded")]
			}

			if (row.period === "30minutes" && row.count > 15) {
				return [false, Response.fail(429, "Rate limit exceeded")]
			}

			if (row.period === "hour" && row.count > 20) {
				return [false, Response.fail(429, "Rate limit exceeded")]
			}
		}

		return [true, null];
	}

	/**
	 * Set the visibility of a comment
	 * @param {int} comment_id
	 * @param {boolean} visibility
	 * @param {Object} session
	 * @returns {Promise.<boolean>} - Whether action was successful
	 */
	async setCommentVisibility(comment_id, visibility, session) {
		if (!session.user || !session.user.id) {
			return false;
		}

		let r;
		if (session.user.is_admin) {
			r = await this.db.query("default", "UPDATE comments SET visible=? WHERE id=?", [comment_id]);
		} else {
			r = await this.db.query("default", "UPDATE comments SET visible=? WHERE id=? AND user_id=?", [comment_id, session.user.id]);
		}

		return (r.affectedRows > 0);
	}

	/**
	 * Set the user's vote for a game
	 * @param {int} game_id
	 * @param {int} rating
	 * @param {Object} session
	 * @returns {Promise.<boolean>} - Whether action was successful
	 */
	async setVote(game_id, rating, session) {
		if (!session.user || !session.user.id) {
			return false;
		}

		let r = await this.db.query("default", "INSERT INTO votes (user_id, game_id, rating) VALUES (?, ?, ?) ON DUPLICATE KEY UPDATE rating=?", [session.user.id, game_id, rating, rating]);

		return (r.affectedRows > 0);
	}

	async updateGameInfo(game_id, name, description, authors, session) {
		if (!session.user || !session.user.id) {
			return false;
		}

		let r;

		if (session.user.is_admin) {
			r = await this.db.query("default", "UPDATE games SET name=?, description=?, authors=? WHERE id=?", [name, description, authors, game_id]);
		} else {
			r = await this.db.query("default", "UPDATE users INNER JOIN users_to_game_info on users.id = users_to_game_info.user_id INNER JOIN games on users_to_game_info.game_id = games.id SET games.name=?, games.description=?, games.authors=? WHERE games.id = ? AND users.id = ?", [name, description, authors, game_id, session.user.id]);
		}

		return (r.affectedRows > 0);
	}

	async incrementVersion(game_id) {
		await this.db.query("default", "UPDATE games SET version = version + 1, date_updated = CURRENT_TIMESTAMP WHERE id = ?", [game_id]);
		let r = await this.db.query("default", "SELECT version FROM games WHERE id = ?", [game_id]);
		return r[0].version;
	}

	/**
	 * Upload a new game file
	 * @param {int} game_id
	 * @param {ArrayBuffer} file
	 * @param {Object} session
	 * @returns {Promise.<boolean>} - Whether action was successful
	 */
	async updateGameFile(game_id, file, session) {
		if (!session.user || session.user.id) {
			return false;
		}

		let r1 = await this.db.query("default", "SELECT count(game_id) AS has_file FROM game_files WHERE game_id = ?", [game_id]);

		let r2 = null;

		if (r1[0].has_file) {
			r2 = await this.db.query("file", "UPDATE (game_files INNER JOIN users_to_game_info ON game_files.game_id=users_to_game_info.game_Id) SET file=? WHERE game_files.game_id = ? AND user_id = ?", [game_id, sesison.user.id]);
		} else {
			// Todo, check for user-game link
			r2 = await this.db.query("file", "INSERT INTO (game_files INNER JOIN users_to_game_info ON game_files.game_id=users_to_game_info.game_Id) (game_id, file) VALUES (?, ?)");
		}

		return (r2.affectedRows > 0);
	}

	/**
	 * Add a new game
	 * @param {string} name
	 * @param {string} description
	 * @param {boolean} is_public
	 * @param {ArrayBuffer} file
	 * @param {Object} session
	 * @returns {Promise.<Response>}
	 */
	async addGame(name, description, is_public, session) {
		/*
			Check request is valid
		 */

		if (!session.user || !session.user.id) {
			return Response.common.no_login;
		}

		if (!name) {
			return Response.common.bad_request;
		}

		/*
			Insert game record.
		 */
		let r1 = await this.db.query("default", "INSERT INTO games (name, description, is_public, organisation_id) VALUES (?, ?, ?, ?)", [name, description, is_public, session.user.organisation_id]);

		if (!r1.insertId) {
			return Response.common.db_failure;
		}

		let gameId = r1.insertId;

		return Response.ok(200, gameId);
	}


	/**
	 * @param game_id
	 * @param data
	 * @param ip
	 * @returns {Promise.<Response>}
	 */
	async addToLeaderboard(game_id, data, ip="UNKNOWN") {

		let r_key = await this.db.query("default", "SELECT leaderboard_secret AS secret FROM games WHERE id = ?", [game_id]);

		if (!r_key) {
			return Response.fail(403, "Leaderboard submissions for this game are currently disabled");
		}

		r_key = r_key[0].secret;

		let expected_hash = crypto.createHash("md5").update(data.name + data.score + r_key).digest("hex");
		
		if (data.hash !== expected_hash) {
			return Response.common.bad_request;
		}

		let r = await this.db.query("default", "INSERT INTO leaderboard (game_id, name, score, ip) VALUES (?, ?, ?, ?)", [game_id, data.name, data.score, ip]);

		if (r && r.affectedRows > 0) {
			return Response.ok();
		} else {
			return Response.common.db_failure;
		}
	}
}

module.exports = Action;
