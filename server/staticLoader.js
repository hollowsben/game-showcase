const fs = require("fs");
const walk = require("walk");
const mime = require("mime");

/**
 * Caches static files from disk into memory and serves them
 */
class StaticLoader {
	/**
	 * Create a new StaticLoader
	 * @constructor
	 */
	constructor() {
		this.cache = {};
	}

	/**
	 * Removes the first directory from a path
	 * @param {string} path
	 * @returns {string}
	 */
	fixPath(path) {
		// Remove first dir from path
		return '/' + path.split(/[/\\]/).splice(1).join('/')
	}

	/**
	 * Loads a directory into the cache
	 * @param {string} dir
	 * @returns {Promise.<void>}
	 */
	async load(dir) {
		let i = 0;

		this.cache = await new Promise((resolve, reject) => {
			const walker = walk.walk(dir);
			let files = {};

			walker.on("file", (root, stats, next) => {
				let fpath = `${root}/${stats.name}`;
				fs.readFile(fpath, (err, data) => {
					if (err) {
						reject(err);
					}

					files[this.fixPath(fpath)] = data;
					i++;
					next();
				})
			});

			walker.on("end", () => resolve(files))
		});

		fs.watch(dir, { recursive: true }, this.reloadFile);

		console.log(`${i} file(s) loaded into cache`);
	}

	/**
	 * Returns the file if loaded
	 * @param {string} path - Relative path to file
	 * @returns {object|null} - JSON {mime, body}
	 */
	getFile(path) {
		if (this.cache.hasOwnProperty(path)) {
			return {
				mime: mime.getType(path),
				body: this.cache[path],
			}
		}

		return null;
	}

	/**
	 * Returns middleware function for Koa to serve requested files
	 * @returns {function({ctx}, {next})}
	 */
	middleware() {
		return (async (ctx, next) => {
			if (this.cache.hasOwnProperty(ctx.url)) {
				ctx.response.set("Content-Type", mime.getType(ctx.url));
				ctx.body = this.cache[ctx.url];
				return;
			}

			// Test for index
			let indexPath = ctx.url + "index.html";
			if (this.cache.hasOwnProperty(indexPath)) {
				ctx.response.set("Content-Type", "text/html");
				ctx.body = this.cache[indexPath];
				return;
			}

			// Does not match a file
			// Pass on to next middleware
			await next();
		});
	}
}

module.exports = StaticLoader;
